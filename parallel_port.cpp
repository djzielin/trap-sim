#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include<windows.h>
 
typedef bool (__stdcall *BFUNCTION)();
typedef void (__stdcall *SSFUNCTION)(short,short);

BFUNCTION  IsInpOutDriverOpen;
SSFUNCTION Out32;

int parallel_port_address=0x378;
bool is_parallel_port_setup=false;

void setup_parallel_port()
{
   HMODULE inpout=LoadLibrary("inpout32.dll");
   
   printf("loading inpout32.dll\n");
   if(inpout==NULL)
   {
      printf("  error loading inpout32.dll!\n");
      exit(1); 
   }
   printf("  SUCCESS!\n");
   
   printf("Mapping functions...\n");
   
   IsInpOutDriverOpen=(BFUNCTION) GetProcAddress(inpout,"IsInpOutDriverOpen");
   if(IsInpOutDriverOpen==NULL)
   {
      printf("  error getting address for IsInpOutDriverOpen\n");
      exit(1);
   }
        
   Out32=(SSFUNCTION)GetProcAddress(inpout,"Out32");
   if(Out32==NULL)
   {
      printf("  error getting address for Out32\n");
      exit(1);
   }
 
   bool openStatus=IsInpOutDriverOpen();
   
   if(openStatus==false)
   {
      printf("  Failed to open parallel port\n");
      exit(1);
   }
   
   is_parallel_port_setup=true;
}

void set_parallel_port_value(int val)
{
   if(is_parallel_port_setup==false)
   {
      setup_parallel_port();   
   }

   if(val<0)
   {
      printf("ERROR: value for parallel port set out of range! %d\n",val);
      val=0;
   }
   if(val>255)
   {
      printf("ERROR: value for parallel port set out of range! %d\n",val);
      val=255;
   }

   Out32(parallel_port_address,val);
   printf("set parallel port to: %d\n",val);

}
   
 