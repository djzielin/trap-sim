@echo off

cd X:\Dive\syzygy\projects\trap-sim\sequence_generator
call X:\Dive\syzygy\projects\trap-sim\sequence_generator\sequence_generator.exe
timeout /t 2

IF "%2"=="" GOTO HAVE_1
GOTO HAVE_2

:HAVE_1
call X:\Dive\syzygy\scripts\szg_mingw_wrapper2.bat trap_sim %1
GOTO END

:HAVE_2
SET /P userid=     "Enter User ID (e.g. s12): " 
set /P userday=    "Enter Day          (1-3): " 
SET /P blocknumber="Enter Block Number (1-8): "
call X:\Dive\syzygy\scripts\szg_mingw_wrapper2.bat trap_sim %1 %blocknumber% %userday% %userid%
GOTO END
:END
