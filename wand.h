#ifndef WAND_DJZ_H
#define WAND_DJZ_H

#include "arMasterSlaveFramework.h"

class wand
{
public:   
   wand();
   void draw();
   void update(arMatrix4 m4, bool do_gun);
   void set_regular_color();
   void set_highlight_color();

   arVector3 get_position();
   arMatrix4 get_matrix();
   arVector3 get_up();
   arVector3 get_forward();
   
   float compute_ray_distance_to_point(arVector3 target);
private:
   arVector3 _pos;
   arMatrix4 _raw;
   arVector3 _forward;
   
   float wand_color[3];
   float wand_length;
   float wand_width;
   int type;
};

#endif





