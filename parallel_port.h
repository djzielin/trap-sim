#ifndef PARALLEL_PORT_H_DJZ
#define PARALLEL_PORT_H_DJZ

void setup_parallel_port();
void set_parallel_port_value(int val);

#endif