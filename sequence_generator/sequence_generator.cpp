#include <random>
#include <iostream>
#include <time.h>
#include <algorithm>

using namespace std;

FILE *out;

class single_trial
{
public:
   int launcher; 
   float angle_offset;
   float delay_time;
   float random_number; //to shuffle  
   
   //from http://stackoverflow.com/questions/1380463/sorting-a-vector-of-custom-objects
   bool operator < (const single_trial & st) const 
   {
      return random_number<st.random_number;
   }
};

vector<single_trial> all_trials;

int main(int argc, char *argv[])
{
   cout << "started random_generator.exe" << endl;
   //from http://stackoverflow.com/questions/15461140/stddefault-random-engine-generato-values-betwen-0-0-and-1-0
   std::default_random_engine generator(time(0));
   std::uniform_real_distribution<double> distribution(0,1);

   out=fopen("60targets.csv","w");
   
   for(int e=0;e<10;e++)
   {
      for(int i=0;i<6;i++)
      {
         single_trial st;
      
         st.launcher=i%6+10; //10,11,12,13,14,15 - new for spring 2017
	  
         st.delay_time=distribution(generator)*0.5f+1.0f;
         st.random_number=distribution(generator);
         st.angle_offset=distribution(generator)*6.0f-3.0f; //get -3 to 3  
      
         all_trials.push_back(st);
      }
      std::sort(all_trials.begin(), all_trials.end());
   
      for(int i=0;i<all_trials.size();i++)
      {
         fprintf(out,"%d,%f,%f\n",all_trials[i].launcher,all_trials[i].delay_time,all_trials[i].angle_offset);      
      }
      fflush(out);
	  all_trials.clear();
   }
   fclose(out);
   printf("writing to file complete! goodbye!\n");
}
   