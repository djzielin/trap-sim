
class trap_task_summary_log_entry
{
public:
   void store_values()
   {
      log_trial_begin=trial_begin;
      log_current_trial=current_trial;
      log_target_slot=target_slot;
	  log_angle_offset=angle_offset;
      log_mode=mode;
      log_visual_radius=visual_radius;
      log_selection_radius=selection_radius;
      log_is_success=is_success;
   
      log_shot_timing[0]=shot_timing[0];
      log_shot_distance[0]=shot_distance[0];
      log_shot_closeness[0]=shot_closeness[0];
      log_shot_result[0]=shot_result[0];
   
      log_shot_timing[1]=shot_timing[1];
      log_shot_distance[1]=shot_distance[1];
      log_shot_closeness[1]=shot_closeness[1];
      log_shot_result[1]=shot_result[1];
      
      log_shot_angular_size[0]=shot_angular_size[0];
      log_shot_angular_size[1]=shot_angular_size[1];
      
      log_closeness_angular_size[0]=fabs(atan(log_shot_closeness[0]/log_shot_distance[0])*180.0f/M_PI);      
      log_closeness_angular_size[1]=fabs(atan(log_shot_closeness[1]/log_shot_distance[1])*180.0f/M_PI);      


   }

   void write_to_file(FILE *f)
   {
      fprintf(f,"%f,%d,%d,%f,%d,%f,%f,%d,", 
                 
      log_trial_begin,
      log_current_trial,
      log_target_slot,
	  log_angle_offset,
      log_mode,
      log_visual_radius,
      log_selection_radius,
      log_is_success);
	  
	 
      fprintf(f,"%f,%f,%f,%d,%f,%f\n",
      log_shot_timing[0], //shot1_time,
      log_shot_distance[0], // shot1_tdist,
      log_shot_closeness[0], //shot1_closeness,
      log_shot_result[0], //shot1_result
   
     // log_shot_timing[1], //shot2_time,
     // log_shot_distance[1], // shot2_tdist,
     // log_shot_closeness[1], //shot2_closeness,
     // log_shot_result[1], //shot2_result
      log_shot_angular_size[0],
     // log_shot_angular_size[1],
      log_closeness_angular_size[0]
     // log_closeness_angular_size[1]      
      );     
   
      fflush(f);
   }
   
private:
   float log_trial_begin;
   int   log_current_trial;
   int   log_target_slot;
   float log_angle_offset;
   int   log_mode;
   float log_visual_radius;
   float log_selection_radius;
   int   log_is_success;
   
   float log_shot_timing[2];
   float log_shot_distance[2];
   float log_shot_closeness[2];
   int   log_shot_result[2];   
   float log_shot_angular_size[2];
   float log_closeness_angular_size[2];
};

vector<trap_task_summary_log_entry *> trap_task_summary_log;

bool summary_dumped=false;
FILE *summary_log;

void log_trial_summary()
{   
   trap_task_summary_log_entry *d=new trap_task_summary_log_entry();
   d->store_values();
   trap_task_summary_log.push_back(d);
}

void dump_summary_log()
{
   if(summary_dumped)
   {
      printf("summary log has already been dumped\n");
      return;
   }

   for(int i=0;i<trap_task_summary_log.size();i++)
   {
      trap_task_summary_log[i]->write_to_file(summary_log); 
   }
   
   printf("dumped %d trials to summary file\n",trap_task_summary_log.size());
   
   summary_dumped=true;   
   fclose(summary_log);
}

void open_summary_log(string filename)
{
   printf("Opening summary file: %s\n",filename.c_str());

   summary_log=fopen(filename.c_str(),"a"); //append just incase somethign went wrong
   if(summary_log==NULL)
   {
      printf("couldn't open %s\n",filename.c_str());
      exit(1);   
   }
   printf("  DONE!\n");
      
   fprintf(summary_log,"trial_begin,current_trial,target_slot,angle_offset,display_mode,visual_radius,selection_radius,is_success,shot1_time,shot1_tdist,shot1_closeness,shot1_result,visual_angular1,closeness_angular1\n"); 

   fflush(summary_log);
}  

///////////////////////////////////////////////////////////////////////////////////////////////

extern int displayed_frame;

class trap_task_detail_log_entry
{
public:
   void store_values(double time_stamp)
   {
      log_time_stamp=time_stamp;
      
      log_current_trial=current_trial;
      log_target_slot=target_slot;
      log_angle_offset=angle_offset;
      log_mode=mode;
      log_visual_radius=visual_radius;
      log_selection_radius=selection_radius;
      log_state=state;
      log_is_success=is_success;
   
      w_pos=duckWand.get_position();
      w_dir=duckWand.get_forward();   
      w_up=duckWand.get_up();

      t_pos=target_pos;

      log_button=button;
      log_closest_point=closest_point;
      log_closeness=how_far_from_target;
      log_target_angular_size=target_angular_size;
      
      log_closeness_angular_size=fabs(atan(log_closeness/current_shot_distance)*180.0f/M_PI);      

      h_pos=duckHead.get_position();
      h_dir=duckHead.get_forward();
      h_up= duckHead.get_up();  
      
      log_flight_time=flight_time_method2;
      log_aclose_threshold=fabs(atan(selection_radius/current_shot_distance)*180.0f/M_PI);      


      log_displayed_frame=displayed_frame;      
   }

   void write_to_file(FILE *f)
   {
     
      fprintf(f,"%f,%d,%d,%f,%f,%f,%d,%d,",
         (log_time_stamp/1000.0),
         log_current_trial,
         log_target_slot,
         log_angle_offset,
         log_visual_radius,
         log_selection_radius,
         log_state,
         log_is_success);
      
       
      dump_arVector3(f,w_pos);
      dump_arVector3(f,w_dir);
      dump_arVector3(f,w_up);
      dump_arVector3(f,t_pos);
      
      fprintf(f,"%d,",log_button);
     
      dump_arVector3(f,log_closest_point);
      
      fprintf(f,"%f,%f,%f,",log_closeness,log_target_angular_size,log_closeness_angular_size);
      
      dump_arVector3(f,h_pos);
      dump_arVector3(f,h_dir);
      dump_arVector3(f,h_up);      
     
      fprintf(f,"%d,%f,%f",log_displayed_frame,log_flight_time,log_aclose_threshold);
      
      fprintf(f,"\n");
      //fflush(f);
   }
   
private:
   void dump_arVector3(FILE *f, arVector3 av)
   {
      fprintf(f,"%f,%f,%f,",av.v[0],av.v[1],av.v[2]);
   }  


   double log_time_stamp;
   int   log_current_trial;
   int   log_target_slot;
   float log_angle_offset;
   int   log_mode;
   float log_visual_radius;
   float log_selection_radius;
   int   log_state;
   int   log_is_success;
   
   arVector3 w_pos;
   arVector3 w_dir;
   arVector3 w_up;
   arVector3 t_pos;
   arVector3 h_pos;
   arVector3 h_dir;
   arVector3 h_up;
   
   
   int log_button;
   arVector3 log_closest_point;
   float log_closeness;
   float log_target_angular_size;
   float log_closeness_angular_size;
   float log_flight_time;
   float log_aclose_threshold;
   
   int log_displayed_frame;
};

vector<trap_task_detail_log_entry *> trap_task_detail_log;

FILE *detail_log;
bool detail_dumped=false;


void log_trial_detail(double time_stamp)
{   
   trap_task_detail_log_entry *d=new trap_task_detail_log_entry();
   d->store_values(time_stamp);
   trap_task_detail_log.push_back(d);
 }



void dump_detail_log()
{
   if(detail_dumped)
   {
      printf("detail log has already been dumped\n");
      return;
   }

   for(int i=0;i<trap_task_detail_log.size();i++)
   {
      trap_task_detail_log[i]->write_to_file(detail_log); 
   }
   
   printf("dumped %d entries to detail file\n",trap_task_detail_log.size());
   fflush(detail_log);
   detail_dumped=true;   
   fclose(detail_log);
}

void open_detail_log(string filename)
{ 
   printf("Opening detail file: %s\n",filename.c_str());
   
   detail_log=fopen(filename.c_str(),"a"); //append just incase somethign went wrong
   if(detail_log==NULL)
   {
      printf("couldn't open %s\n",filename.c_str());
      exit(1);   
   }
    printf("  DONE!\n");
   
   fprintf(detail_log,"time_stamp,current_trial,target_slot,angle_offset,visual_radius,selection_radius,state,is_success,");
   fprintf(detail_log,"w_posX,w_posY,w_posZ,w_dirX,w_dirY,w_dirZ,w_upX,w_upY,w_upZ,");
   fprintf(detail_log,"t_posX,t_posY,t_posZ,");
   fprintf(detail_log,"button,");
   fprintf(detail_log,"cp_posX,cp_posY,cp_posZ,closeness,target_angular_size,closeness_angular_size,");
   fprintf(detail_log,"h_posX,h_posY,h_posZ,h_dirX,h_dirY,h_dirZ,h_upX,h_upY,h_upZ,displayed_frame,flight_time,aclose_threshold");
   fprintf(detail_log,"\n");
   fflush(detail_log);
   
   trap_task_detail_log.reserve(100000); 
   printf("detail log vector capacity: %d\n",trap_task_detail_log.capacity());
   printf("detail log vector max_size: %d\n",trap_task_detail_log.max_size());
     
}
 
