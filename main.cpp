#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/wglew.h>

#include "arMasterSlaveFramework.h"
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>

#include "opengl_shapes.h"
#include "arGlut.h"
#include "wand.h"
#include "headlamp.h"
#include "trap_task.h"
#include "frame_buffer.h"

#define BACKGROUND_COLOR 0.05f,0.05f,0.05f,0.0f

using namespace std;

sphere *ourSphereHi;

int mode=0; //passed in from command line
int task=4;
int display_order=4;

string config_file="X:/Dive/syzygy/projects/trap-sim/configs/default_config.txt";
int blocknumber=0;
string user_day="1";
string user_id="z0";
string log_name;

int shown_frames=6;
int hidden_strobe_frames;
int total_strobe_frames;

frame_buffer *fb_left;
frame_buffer *fb_right;

bool show_diagnostics=true;
bool isFrontWall=false;

void init_glew()
{
   GLenum err = glewInit();
   if (GLEW_OK != err)
   {
      // Problem: glewInit failed, something is seriously wrong. 
      fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
      exit(1);
   }
   fprintf(stdout, "Using GLEW %s\n", glewGetString(GLEW_VERSION));
}

//experiments to force v-sync. probably unnecessary
void force_vsync()
{
   printf("Forcing V-Sync\n");
#ifdef WGL_EXT_swap_control
   int current_swap=wglGetSwapIntervalEXT();
   printf("  Swap currently set to: %d\n",current_swap);
   
   bool result=wglSwapIntervalEXT(1);
   printf("  wglSwapIntervalEXT(1) returns: %d\n",result);
#else 
   printf("  No swap control available\n");
#endif
}


GLfloat  position_0[4] = { 0.0, 1, 0.0, 0.0 }, // 0 = directional, 1 = location  //TODO, this works, but doesn't make sense. 
         ambient_0[4] =  { 0.4, 0.4, 0.4, 1.0 },
         diffuse_0[4] =  { 1.0, 1.0, 1.0, 1.0 },  
         specular_0[4] = { 0.0, 0.0, 0.0, 0.0 };
 
 void setup_lights()
 {
   glLightfv(GL_LIGHT0, GL_POSITION, position_0);
   glLightfv(GL_LIGHT0, GL_AMBIENT,  ambient_0);  
   glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuse_0);  
   glLightfv(GL_LIGHT0, GL_SPECULAR, specular_0); 
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);
 
 }
 
string get_current_date_and_time() //TODO move to seperate (utility?) file
{
   time_t t=time(0);

   struct tm *now=localtime(&t);

   char ret_val[1000];
   //sprintf(ret_val,"Y%d_M%d_D%d_H%d_M%d_S%d",now->tm_year+1900,now->tm_mon+1,now->tm_mday,now->tm_hour,now->tm_min,now->tm_sec);
   sprintf(ret_val,"%04d%02d%02d%02d%02d%02d",now->tm_year+1900,now->tm_mon+1,now->tm_mday,now->tm_hour,now->tm_min,now->tm_sec); //more compact

   cout << "current date and time is: "  << ret_val << endl;

   return string(ret_val);
}

string get_logfile()
{
   char block_as_char[100];
   sprintf(block_as_char,"%d",blocknumber+1);
   return user_id+"_d"+user_day+"_b"+block_as_char; //+"_"+get_current_date_and_time();
}

 
void init (arMasterSlaveFramework &fw,  arGUIWindowInfo* gwi)
{   
   char szPath[128] = "";
   gethostname(szPath, sizeof(szPath));
   cout << "host name: " << szPath << endl;	  
   if(strcmp(szPath,"divefront")==0)
      isFrontWall=true;
	  
   setup_lights();  

   glShadeModel(GL_SMOOTH);
   glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
   glEnable(GL_COLOR_MATERIAL);
   glEnable(GL_LINE_SMOOTH);
   glEnable(GL_POINT_SMOOTH);
   glEnable(GL_NORMALIZE); 
   glEnable(GL_DEPTH_TEST);
  // glEnable(GL_CULL_FACE);
   
   ourSphereHi=new sphere(4);
   
   init_glew();
   force_vsync();
   

   
   //if(task==0) setup_selection_task(display_order,mode);   //TODO really need to extend a prototype task, and avoid all the if statements everywhere.
   //if(task==1) setup_navigation_task(display_order,mode);
   //if(task==2) setup_riding_task(display_order,mode);
   //if(task==3) setup_moving_task(display_order,mode);   
   if(task==4) setup_trap_task(fw,config_file,blocknumber);   
   
   fw.setClipPlanes(0.1,350);
   
   fb_left=new frame_buffer(gwi->getSizeX(),gwi->getSizeY());
   fb_right=new frame_buffer(gwi->getSizeX(),gwi->getSizeY());
   printf("done with graphics init\n");
}

////////////////////////////////////////////////////////////////////////////////
// main draw-render loop.
////////////////////////////////////////////////////////////////////////////////

unsigned int total_frames=0;
unsigned int pair_frames=0;

#define CSCALE   0.00025

void render_text_helper(string text, float x, float y, float char_scale=CSCALE)
{   
   glColor3f(0.5f,0.5f,0.5f);

   glDisable(GL_LIGHTING);
   glDisable(GL_DEPTH_TEST);
   
   glMatrixMode(GL_PROJECTION); 
   glPushMatrix();
   glLoadIdentity();
   
   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();
   glLoadIdentity();
   
   glPushMatrix();
      glTranslatef(x,y,.000001);
      glScalef(char_scale,char_scale,char_scale);
      
      for (int e=0;e<text.size();e++) 
         glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[e]);

   glPopMatrix();
   
   glMatrixMode(GL_PROJECTION); 
   glPopMatrix();
   
   glMatrixMode(GL_MODELVIEW);
   glPopMatrix();
  
   glEnable(GL_DEPTH_TEST);
   glEnable(GL_LIGHTING);
}

void render_3d_text(string text, float x, float y, float z, float char_scale=CSCALE)
{
   glColor3f(0.5f,0.5f,0.5f);

   glDisable(GL_LIGHTING);
   //glDisable(GL_DEPTH_TEST);
   
   //glMatrixMode(GL_PROJECTION); 
   //glPushMatrix();
   //glLoadIdentity();
   
   //glMatrixMode(GL_MODELVIEW);
   //glPushMatrix();
   //glLoadIdentity();
   
   glPushMatrix();
      glTranslatef(x,y,z);
      glScalef(char_scale,char_scale,char_scale);
      
      for (int e=0;e<text.size();e++) 
         glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[e]);

   glPopMatrix();
   
   //glMatrixMode(GL_PROJECTION); 
   //glPopMatrix();
   
   //glMatrixMode(GL_MODELVIEW);
   //glPopMatrix();
  
   //glEnable(GL_DEPTH_TEST);
   glEnable(GL_LIGHTING);

}

void render_text(string text, float x, float y)  //TODO move to seperate (utility?) file
{
   if(isFrontWall==false) return;
   
   if(show_diagnostics==false) return;

   render_text_helper(text,x,y,CSCALE);
}

void render_text_always(string text, float x, float y)  //TODO move to seperate (utility?) file
{
   if(isFrontWall==false) return;

   render_text_helper(text,x,y,CSCALE);
}

void render_text_always_little_bigger(string text, float x, float y)  //TODO move to seperate (utility?) file 
{
   if(isFrontWall==false) return;

   render_text_helper(text,x,y,CSCALE*1.5f);

}

void render_text_always_bigger(string text, float x, float y)  //TODO move to seperate (utility?) file 
{
   if(isFrontWall==false) return;

   render_text_helper(text,x,y,CSCALE*2);

}

int repeated_frames=5;
int fcount=0;
 
bool seenLeft;
bool seenRight;
int displayed_frame=0;
 
void draw_floor_cross()
{
   glColor3f(1,1,1);
   glDisable(GL_LIGHTING);  //draw cross on ground
   glPushMatrix();
   glBegin(GL_LINES);
   glVertex3f(0,0.01,-2);
   glVertex3f(0,0.01,2);
   glVertex3f(-2,0.01,0);
   glVertex3f(2,0.01,0);
   glEnd();
   glPopMatrix();
   glEnable(GL_LIGHTING);	
}
 
void display(arMasterSlaveFramework& fw, arGraphicsWindow& gw, arViewport& vp) //try the new style draw callback (ie, get more parameters)
{
   frame_buffer *fb;
   
   //////////// CLEAR BUFFERS /////////////////////////
   
   float eye_sign=gw.getCurrentEyeSign();
   //printf("eye sign: %f\n",eye_sign);
 
   char eye='M';

   
   if(eye_sign==-1.0f)
   {
      eye='L'; 
     // printf("Render Eye Left!\n");
      seenLeft=true;
      fb=fb_left;
   }
   if(eye_sign==1.0f)
   {
      eye='R'; 
     // printf("Render Eye Right!\n");
      seenRight=true;
      fb=fb_right;
   }
   if(eye_sign==0.0f)
   {
      eye='M'; 
      //printf("Eye Center (non-stereo)\n");
      fb=fb_left;
   }
	    
	//if(task==1)
   //   draw_navigation_cross();  //we need to do this before we apply navigation translations, so cross travels with us
   //if(task==2)
   //   draw_riding_cross();
      
   //if(task==1 || task==2)
   //   fw.loadNavMatrix(); //allows us to "drive" around our world
   
   if(fcount==0 || (mode==3 && fcount<shown_frames))  // && eye_sign==-1.0f) //add back in to make mono
   {      
      //printf("attemping to bind\n");
      fb->bind();
      
      //if(task==1 || task==2)
      //    glClearColor(BACKGROUND_COLOR); //world background color dark gray
      //else
      displayed_frame=1;
   
      ////////// SETUP LIGHTS /////////////////////////////
   
      if(task==4)
          headlamp(fw);
      else
          setup_lights();
            
      //if(task==0) draw_selection_task();
      //if(task==1) draw_navigation_task();
      //if(task==2) draw_riding_task();  
      //if(task==3) draw_moving_task(); 
      if(task==4) draw_trap_task();   

      fb->unbind();
      
   }
         
   fb->draw();
   
   if(mode==1 && fcount!=0) 
      displayed_frame=2;
      
   if(mode==0)
      render_text("[0] HFR",0.0,0.8);
   if(mode==1)
      render_text("[1] HP",0.0,0.8);
   if(mode==2)
   {
     if(fcount>0) 
	  {
         //printf("frame: %d - HIDING\n",fcount);
         glClearColor(BACKGROUND_COLOR); //world background color 
         glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
         //printf("  BLANKING\n");
         displayed_frame=0;
	  }

     render_text("[2] LP",0.0,0.8);
   }
   if(mode==3) //strobe mode
   {
      if(fcount>=shown_frames)
      {
         glClearColor(BACKGROUND_COLOR); //world background color 
         glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
         displayed_frame=0;
       
      }
      
      render_text("[3] SS",0.0,0.8);
   }
   
   if((mode<3) && (mode!=0))
   {
      char timing[1000];
      sprintf(timing,"REP: %d FPS: %.02f",repeated_frames,(60.0/((float)repeated_frames+1.0)));
      render_text(timing,0.0,0.7);
   }
   if(mode==3)
   {
      char timing[1000];
      char timing2[1000];
      
      float ms_per_frame=1.0/60.0*1000.0;
      sprintf(timing, "OPEN  : %02d MS: %0.02f",shown_frames,ms_per_frame*(float)shown_frames);
      sprintf(timing2,"CLOSED: %02d MS: %0.02f",hidden_strobe_frames,ms_per_frame*(float)hidden_strobe_frames);
      render_text(timing,0.0,0.7);
      render_text(timing2,0.0,0.6);
   }
   
    /*char speed_txt[1000];
      sprintf(speed_txt,"SPEED: %.1f ft/s %.1f mph",target_speed,target_speed*0.68181818181818);
      render_text(speed_txt,0.0,0.9);*/

   
   //if(leftEye) //to make mono
   //{
   //
   //      glClearColor(0.0,0.0,0.0,1.0); //world background color 
   //      glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
 // 
   //}
   
   //char buffer[100];
   //sprintf(buffer,"%c%d%d",eye,fcount,total_frames%10);
   //render_text(buffer,0.58,-0.43); //for high speed camera verification

   if((seenLeft && seenRight) || eye_sign==0.0f) //have we gotten a stereo pair? 
   {
      pair_frames++;
	  
      seenLeft=false;
      seenRight=false;

      fcount++;
      
      if(mode<3)
      {
         if(fcount>repeated_frames || mode==0)
         {
            fcount=0;
         }
      }
      else if(mode==3)
      {
         if(fcount>=total_strobe_frames)
         {
            fcount=0;
         }
      
      }
   }  
   
   total_frames++;
   //glFlush(); //is this neccisarry?
   
   //ar_usleep(2000); //if we want real judder/delay  
}

double seconds_since_update=0;

int last_fcount=-1;

void preExchange(arMasterSlaveFramework& fw) //happens only on master
{
   //int button_num=fw.getNumberButtons();
   //int axis_num=fw.getNumberAxes();
   //printf("Master Number buttons: %d axes: %d\n",button_num,axis_num);


   /*if(mode!=0 && repeated_frames>0) //if we want to make sure we get preExchanges matches to frame display
      if(fcount==last_fcount)
         printf("ERROR: called preexchange twice for single frame rendered\n");

   last_fcount=fcount;
         
   double delta_milliseconds=fw.getLastFrameTime();
   double delta_seconds=delta_milliseconds/(double)1e3;
   //printf("last frame: %f\n",delta_seconds);
   seconds_since_update+=delta_seconds;
   
  */
  
   //Now we aren't skipping any updates
   //if((mode<3 && fcount>0) || (mode==3 && fcount>shown_frames))
   //{
   //  //printf("skipping preexchange updates\n");
   //  return;
   //}
   
   /*if(task==1) //only needed for tasks that have navigation
   {
      update_master_navigation_task(seconds_since_update,fw);
   }
   if(task==2)
   {
      update_master_riding_task(seconds_since_update,fw);
   }
   
   seconds_since_update=0;  */ 
}

double post_seconds_since_update=0;



void postExchange(arMasterSlaveFramework& fw)
{ 
   //printf("---------------postExchange----------------------\n");

   double delta_milliseconds=fw.getLastFrameTime();
   double delta_seconds=delta_milliseconds/(double)1e3;
  
   //if(fw.getOnButton(0)) //if we want to be able to change mode inside task
   //   mode++;
   //if(fw.getOnButton(1)) 
   //   mode--;
      
   if(mode<0) mode=0;
   if(mode>3) mode=3;   
   
   //if(fw.getOnButton(3)) //if we want to be able to change mode inside task
   //   repeated_frames++;
   //if(fw.getOnButton(2)) 
   //   repeated_frames--;
    
   //if(repeated_frames<1)
   //   repeated_frames=1;
	  
	/*float x_axis=fw.getAxis(0);
	//target_speed+=x_axis*delta_seconds*10.0f;
	//if(target_speed<1.0f)
	//   target_speed=1.0f;
   if(x_axis>0.1)
      target_speed=target_speed_choices[1];
   if(x_axis<-0.1)
      target_speed=target_speed_choices[0];   
    */
    
    post_seconds_since_update+=delta_seconds; 
    
   if(mode==3)
   {
      hidden_strobe_frames=(repeated_frames-3)*shown_frames;
      if(hidden_strobe_frames<shown_frames)
         hidden_strobe_frames=shown_frames;
         
      total_strobe_frames=hidden_strobe_frames+shown_frames;   
   }
   
   //if((mode<3 && fcount>0) || (mode==3 && fcount>=shown_frames))
   //   return;
         
   //printf("-------------actually doing update!--------------\n");      
         
   //if(task==0) update_selection_task(fw);
   //if(task==1) update_navigation_task(fw);
   //if(task==2) update_riding_task(fw);
   //if(task==3) update_moving_task(fw,post_seconds_since_update);
   if(task==4) update_trap_task(fw,post_seconds_since_update);   
   
   post_seconds_since_update=0;
}

void cleanup(arMasterSlaveFramework& fw)
{
   printf("exit has been requested.\n");
   //if(task==0) cleanup_selection();
   //if(task==1) cleanup_navigation();
   //if(task==2) cleanup_riding();
   //if(task==3) cleanup_moving();
   if(task==4) cleanup_trap_task(fw);
}

int main(int argc, char** argv)
{
   if(argc>1)
   {
      config_file=argv[1];
      printf("got config file from command line: %s\n",config_file.c_str());
   }
   if(argc>2)
   {
      blocknumber=atoi(argv[2])-1;
      if(blocknumber<0) 
      {
         printf("bad block number! (should be range (1-7)\n");    
         return 1;         
      }
      printf("user is specifying block number: %d\n",blocknumber+1);
   }
   
   if(argc>3)
   {
      user_day=argv[3];
      printf("user is specifying user_day number: %s\n",user_day.c_str());
   }
   if(argc>4)
   {
      user_id=argv[4];
      printf("user is specifying user_id number: %s\n",user_id.c_str());
   }   

  arMasterSlaveFramework framework;

  framework.setWindowStartGLCallback(init);
  framework.setPreExchangeCallback(preExchange);
  framework.setPostExchangeCallback(postExchange);
  framework.setDrawCallback(display);
  framework.setExitCallback(cleanup);
  //framework.setKeyboardCallback(keyboard); //TODO: get rid of this in production version
  
  //framework.setClipPlanes(0.1, 2000);
  framework.setUnitConversion(1.0);
  framework.usePredeterminedHarmony(); //don't start running draw/pre/post until all nodes connected
  
  if (!framework.init(argc, argv))    return 1;
  if (!framework.start())             return 1;

  return 0; // Never get here.
}





