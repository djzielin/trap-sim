#ifndef FRAME_BUFFER_DJZ
#define FRAME_BUFFER_DJZ


class frame_buffer 
{


public:
	frame_buffer(int xsize, int ysize)  
	{
		x=xsize;
      y=ysize;
      isSetup=false;
      
      printf("creating frame buffer for size: %d %d\n",x,y);
	} 
	void bind();
	void unbind();
	void draw();

private:

	int x,y;
   bool isSetup;
	void initialize();

   
	GLuint textureName;
	GLuint frameBuffer;
   GLuint depth_rb;
};


#endif

