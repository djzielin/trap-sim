#ifndef CUBE_DZ_H
#define CUBE_DZ_H

#include <GL/gl.h>
#include <stdio.h>
#include <stdlib.h>

void GL_draw_cube();
void GL_draw_cube_wireframe();

#endif
