#include "arPrecompiled.h"
#include "arMasterSlaveFramework.h"

void headlamp(arMasterSlaveFramework& fw)
{
   ///////////////////// SETUP LIGHTS //////////////////////////////////////////
   GLfloat  position_0[4] = { 0.0, 5.0,0.0, 1.0 }, // 0 = directional, 1 = location
            ambient_0[4] =  { 0.2, 0.2, 0.2, 1.0 },
            diffuse_0[4] =  { 1.0, 1.0, 1.0, 1.0 },
            specular_0[4] = { 0.0, 0.0, 0.0, 0.0 };

   glPushMatrix();
      //glMultMatrixf(fw.getMidEyeMatrix().v);
      
      glLightfv(GL_LIGHT0, GL_POSITION, position_0);
      glLightfv(GL_LIGHT0, GL_AMBIENT,  ambient_0);  //TODO probably only need to do this once?
      glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuse_0);  //
      glLightfv(GL_LIGHT0, GL_SPECULAR, specular_0); //
      glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
   glPopMatrix();
}
