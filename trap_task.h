#ifndef TRAP_TASK_DJZ_H
#define TRAP_TASK_DJZ_H

#include "arMasterSlaveFramework.h"

//TODO: make these all part of an experiment "prototype class", to facilitate different tasks 
void setup_trap_task(arMasterSlaveFramework& fw, string config_file, int blocknumber);
void draw_trap_task();
void update_trap_task(arMasterSlaveFramework& fw, double t);
void cleanup_trap_task(arMasterSlaveFramework& fw);

#endif