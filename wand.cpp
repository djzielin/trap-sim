#include "wand.h"
#include <GL/gl.h>
#include "opengl_shapes.h"
#include "arGlut.h"

wand::wand()
{
   set_regular_color();
   //wand_length=500.0;
   wand_length=1.0f;
   type=1;
   wand_width=0.3;
}

void wand::draw()
{
   //glColor3f(wand_color[0], wand_color[1], wand_color[2]);

   if(type==0 || type==2)
   {
      glLineWidth(1);
      glDisable(GL_LIGHTING);
      
      glPushMatrix();
         glMultMatrixf(_raw.v);
         glBegin(GL_LINES);
            glVertex3f(0.0f,0.0f,0.0f); //-wand_length*0.5f);
            glVertex3f(0.0f,0.0f,-wand_length);
         glEnd();
      glPopMatrix();
      
      //glLineWidth(1);
      glEnable(GL_LIGHTING);
   }

   if(type==1)
   {      
      //glDisable(GL_LIGHTING);

      glPushMatrix();
         glMultMatrixf(_raw.v);
         glTranslatef(0.0f, 0.0f, wand_length*-0.5f-0.1f);
         glScalef(wand_width*0.25f,  wand_width*0.25f, wand_length*0.5); 
         GL_draw_pyramid();
      glPopMatrix();      
      //glEnable(GL_LIGHTING);
	  
   }
   if(type==2)
   {
      glPushMatrix();
         glMultMatrixf(_raw.v);
         glRotatef(180,0,1,0);
         //glTranslatef(0.0f, 0.0f, wand_length*-0.5f);
         glutSolidCone(wand_width*0.5,wand_length,50,50);  
      glPopMatrix();      
   }
   if(type==3)
   {
   
      double plane[4] = {0.0, 1.0, 0.0, 0.0}; 
	   glEnable(GL_CLIP_PLANE0);
	   glClipPlane(GL_CLIP_PLANE0, plane);
	   //RenderWorld(); 
	   
   
      // http://www.andersriggelsen.dk/glblendfunc.php
      //glEnable(GL_BLEND);
      //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      
      //glBlendEquation(GL_FUNC_ADD);

      glDisable(GL_LIGHTING);

      glLineWidth(2);    
      
      
      
       
      glPushMatrix();
         glMultMatrixf(_raw.v);
         glBegin(GL_LINES);
            
            glVertex3f(0.0f,0.0f,0.0f); //-wand_length*0.5f);
            glColor4f(0.75,0,0,1.0); //was 0.5
            glVertex3f(0.0f,0.0f,-wand_length);
         glEnd();
      glPopMatrix();
      
      
      
      glLineWidth(1);
      //glDisable(GL_BLEND);
      glEnable(GL_LIGHTING);
      glDisable(GL_CLIP_PLANE0);
   }
}

void wand::update(arMatrix4 m4, bool do_gun=false)
{   
   _raw=m4;
   if(do_gun==false)
     _raw=_raw*ar_rotationMatrix('x', ar_convertToRad(-45.0f)); //rotate down 15 degrees?
   //_raw_fixed=ar_matrixToNavCoords(_raw); //get wands position in world coordinates if using travel
   _pos=ar_extractTranslation(_raw);
   _forward=get_forward();
}

arVector3 wand::get_position()
{ 
   return _pos;
}

arMatrix4 wand::get_matrix()
{
   return _raw;
}

arVector3 wand::get_forward()
{
   return _raw*arVector3(0,0,-1) - _raw*arVector3(0,0,0);
}

arVector3 wand::get_up()
{
   return _raw*arVector3(0,1,0) - _raw*arVector3(0,0,0);
}


float wand::compute_ray_distance_to_point(arVector3 target)
{
   arVector3 closest_point=ar_projectPointToLine(_pos,_forward,target);
   arVector3 distance=target-closest_point;
   float mag=distance.magnitude();
   
   return mag;
}

void wand::set_regular_color()
{
   wand_color[0]=1;
   wand_color[1]=1;
   wand_color[2]=1;
}

void wand::set_highlight_color()
{
   wand_color[0]=1;
   wand_color[1]=1;
   wand_color[2]=1;  
}
