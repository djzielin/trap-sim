#note for SS its repeated_frames-3=units closed (each unit is 100ms)
repeated_frames 5
show_diagnostics 0

show_remaining 0

target_speed 95.33


selection_radius 2.0
visual_radius 2.0

# HFR=0 HP=1 LP=2 SS=3
mode 0

target_file X:/Dive/syzygy/projects/trap-sim/sequence_generator/100right_targets.csv

log_prefix  X:/Dive/syzygy/projects/trap-sim/logs/tutorial/

#new stuff
do_gun 1
do_shatter_sound 0
show_ray 1
show_targeting_sphere 1
targeting_size 4
sound_prefix X:/Dive/syzygy/projects/trap-sim/sounds/
results_time 2.0

hit_color 0.0 1.0 0.0
miss_color 1.0 0.0 0.0
