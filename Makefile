VPATH = .

include $(SZGHOME)/build/make/Makefile.os
include $(SZGHOME)/build/make/Makefile.vars
	
# old tasks from pervious experiments   
# walk_navigation selection_task navigation_task riding_task moving_task
   
OBJSRAW =  main opengl_shapes wand headlamp trap_task frame_buffer head parallel_port
OBJS = $(OBJSRAW:%=%$(OBJ_SUFFIX))
ALL = trap_sim$(EXE)

include $(SZGHOME)/build/make/Makefile.rules

SZG_INCLUDE+=-w

SZG_INCLUDE+=-I./glew-1.10.0/include
SZG_LINK_LIBS:= ./glew-1.10.0/lib/libglew32.a $(SZG_LINK_LIBS)

trap_sim$(EXE): $(OBJS) $(SZG_LIBRARY_DEPS)
	$(SZG_USR_FIRST) $(OBJS) $(SZG_USR_SECOND)
	$(COPY)


