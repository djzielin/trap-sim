#ifndef HEAD_DJZ_H
#define HEAD_DJZ_H

#include "arMasterSlaveFramework.h"

class head
{
public:   
   head();

   void update(arMatrix4 m4);
   arVector3 get_position();
   arMatrix4 get_matrix();
   arVector3 get_up();
   arVector3 get_forward();
   
private:
   arVector3 _pos;
   arMatrix4 _raw;
   arVector3 _forward;

};

#endif





