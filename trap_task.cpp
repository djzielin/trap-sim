#include "trap_task.h"
#include "wand.h"
#include "head.h"
#include "opengl_shapes.h"
#include "parallel_port.h"

static arVector3 target_color;
static int target_slot;
static int current_trial=-1;
float target_speed=95.33;

float visual_radius=1.0f; 
float selection_radius=1.0f;

extern int mode; 
extern int repeated_frames;
extern bool show_diagnostics;

wand duckWand;
head duckHead;

sphere *claySphere; 

int our_block=0;
int number_of_success_shots=0;

//static FILE *fout;
//static FILE *fout_detail;
string target_file;
string log_prefix;
string sound_prefix;
static arVector3 wand_pos; //so we can display wand pos
static bool show_wand_pos=false;
static float wand_height=3.0; //this will get overwritten in the calibration phase
arVector3 wand_position_at_hit;
arVector3 targeting_sphere_result;
bool do_gun=false;

static bool button_previous=false;
//static bool button2_previous=false;
//static bool button3_previous=false; 
//static bool buttonX_previous=false;

double trial_begin;
float explosion_expansion=0.0;
float distance_traveled=0;
float distance_traveled_from_user=0;
float target_flight_time=0;
float ring_height=0;
bool is_success=false;
float shot_timing[2]={0,0};
float shot_distance[2]={0,0};
float shot_angular_size[2]={-1,-1};
float shot_closeness[2]={-1,-1};
float current_shot_distance=-1;
float flight_time=0;
int shot_result[2]={-1,-1};
bool out_of_range=false;
int shots_taken=0;
bool target_past_ring=false;
float local_speed;
bool touching_trap_house=false;
float how_far_from_target=-1;
float target_angular_size=-1;
bool show_remaining=true;
float flight_time_method2;
bool do_shatter_sound=true;
bool do_shatter_graphics=true;
int shots_allowed=1;
bool show_ray=true;
//bool show_laser=true;
bool show_targeting_sphere=true;
int targeting_size=9;

float hit_color[3]={255.0f/255.05,116.0f/255.0f,59.0f/255.0f};
float miss_color[3]={255.0f/255.05,116.0f/255.0f,59.0f/255.0f};

bool button;

#define PRERESTING_STATE      -3
#define PRECALIBRATION_STATE  -2
#define CALIBRATION_STATE     -1
#define TARGET_WAITING         0
#define TARGET_MOVING          1
#define TARGET_RESULTS         2
//#define TARGET_OUT_OF_RANGE    3
#define TARGET_ALL_COMPLETE    4
#define TARGET_ALL_DATA_SAVED  5
#define EXTRA_WAITING          6

static int state=PRERESTING_STATE; 
//static int state=TARGET_WAITING; //if we want to skip calibration stage

int prev_state=state;

arVector3 launch_pos;
static arVector3 target_pos; 
arVector3 target_vector;
float angle_offset;
arVector3 closest_point=arVector3(-1,-1,-1);

bool first_time=true;
float downward_velocity=0;
float gravity=32.174f;
float time_to_show_results=2.0f;

extern void render_text(string text, float x, float y);
extern void render_text_always(string text, float x, float y);
extern void render_text_always_little_bigger(string text, float x, float y);
extern void render_text_always_bigger(string text, float x, float y);
extern void render_3d_text(string text, float x, float y, float z, float char_scale);

extern void draw_floor_cross();
//extern string get_current_date_and_time();
extern string get_logfile();

void log_trial_summary();

vector<int> target_vec; //TODO wrap this up in a class
vector<float> delay_vec;
vector<float> offset_vec;

bool block_done_sent=false;
bool isWandReady=false;
float sizeafter6=-1.0f;

///// ORIGINAL CODES /////

#define CODE_ENTERED_WAIT_FOR_LAUNCH 1
#define CODE_BEGIN_TRAP_TOUCH 2
#define CODE_STOP_TRAP_TOUGH 3
#define CODE_REST_START 4
#define CODE_REST_END   5

#define CODE_LAUNCH_0 10
//      CODE_LAUNCH_1 11
//      CODE_LAUNCH_2 12
//      CODE_LAUNCH_3 13
//      CODE_LAUNCH_4 14
//      CODE_LAUNCH_5 15
//      CODE_LAUNCH_6 16
//      CODE_LAUNCH_7 17
//      CODE_LAUNCH_8 18
//      CODE_LAUNCH_9 19

#define CODE_FIRST_HIT 30
#define CODE_FIRST_MISS 31

#define CODE_NO_SHOT_TAKEN 32

#define CODE_SECOND_HIT 40
#define CODE_SECOND_MISS 41

#define CODE_TARGET_OUT_OF_RANGE 50

///// NEW CODES BELOW //////

#define CODE_START_BLOCK_1 51
//      CODE_START_BLOCK_2 52
//      CODE_START_BLOCK_3 53
//      CODE_START_BLOCK_4 54
//      CODE_START_BLOCK_5 55
//      CODE_START_BLOCK_6 56
//      CODE_START_BLOCK_7 57     
//      CODE_START_BLOCK_8 58 
  
#define CODE_END_BLOCK_1   61
//      CODE_END_BLOCK_2   62
//      CODE_END_BLOCK_3   63
//      CODE_END_BLOCK_4   64
//      CODE_END_BLOCK_5   65
//      CODE_END_BLOCK_6   66
//      CODE_END_BLOCK_7   67

#define CODE_TRIAL_PRELAUNCH_1  71
//      CODE_TRIAL_PRELAUNCH_2  72
//      CODE_TRIAL_PRELAUNCH_3  73
//      ...
//      CODE_TRIAL_PRELAUNCH_50 120

int parallel_code=0;
int prev_parallel_code=0;

class target_launcher
{
private:
   arVector3 pos;
   float ddir_angle;
   arVector3 dup;
public:   
   target_launcher(arVector3 p, float dd, arVector3 du) { pos=p; ddir_angle=dd; dup=du; }  
   
   void launch(float offset_angle)
   {
      float final_angle=ddir_angle+offset_angle;
      
      arVector3 v=arVector3(sin(final_angle*M_PI/180.0f),0,-cos(final_angle*M_PI/180.0f));
     
      launch_pos=pos;
      target_pos=pos;
      target_vector=(v+dup).normalize();
      downward_velocity=0;   
   }
};
   
vector <target_launcher *> target_launcher_vec;

#include "trap_task_logging.hpp"

void process_config_pair(string object, string value)
{
   //cout << "  process_config_pair: " << object << " " << value << endl;
   
   if(object=="target_speed")
   {
      target_speed=atof(value.c_str());
      printf("  target_speed is %f\n",target_speed);
   }
  
   if(object=="selection_radius")
   {  
      selection_radius=atof(value.c_str());
      printf("  selection_radius is %f\n",selection_radius);
   }
   
   if(object=="visual_radius")
   {  
      visual_radius=atof(value.c_str());
      printf("  visual_radius is %f\n",visual_radius);
   }
   
   if(object=="sizeafter6")
   {  
      sizeafter6=atof(value.c_str());
      printf("  sizeafter6 is %f\n",sizeafter6);
   }
   if(object=="do_gun")
   {  
      do_gun=atoi(value.c_str());
      printf("  do_gun is %f\n",do_gun);
   }
   
   if(object=="mode")
   {  
      mode=atoi(value.c_str());
      printf("  mode is %d\n",mode);
   }
   
   if(object=="show_diagnostics")
   {  
      show_diagnostics =atoi(value.c_str());
      printf("  show_diagnostics  is %d\n",show_diagnostics );
   }
   
   if(object=="repeated_frames")
   {  
      repeated_frames=atoi(value.c_str());
      printf("  repeated_frames is %d\n",repeated_frames);
   }
   
   if(object=="target_file")
   {  
      target_file=value;
      printf("  target_file is %s\n",target_file.c_str());
   }
   
   if(object=="log_prefix")
   {  
      log_prefix=value;
      printf("  log_prefix is %s\n",log_prefix.c_str());
   }
   if(object=="sound_prefix")
   {  
      sound_prefix=value;
      printf("  sound_prefix is %s\n",sound_prefix.c_str());
   }
   if(object=="show_remaining")
   {
      show_remaining=atoi(value.c_str());
      printf("  show_remaining is: %d\n",show_remaining);   
   }
   if(object=="do_shatter_sound")
   {
      do_shatter_sound=atoi(value.c_str());
      printf("  do_shatter_sound is: %d\n",do_shatter_sound);   
   }
   if(object=="do_shatter_graphics")
   {
      do_shatter_graphics=atoi(value.c_str());
      printf("  do_shatter_graphics is: %d\n",do_shatter_graphics);   
   }
   if(object=="shots_allowed")
   {
      shots_allowed=atoi(value.c_str());
      printf("  shots_allowed is: %d\n",shots_allowed);   
   }
   if(object=="show_targeting_sphere")
   {
      show_targeting_sphere=atoi(value.c_str());
      printf("  show_targeting_sphere is: %d\n",show_targeting_sphere);   
   }
   if(object=="targeting_size")
   {
      targeting_size=atoi(value.c_str());
      printf("  targeting_size is: %d\n",targeting_size);   
   }
   if(object=="show_ray")
   {
      show_ray=atoi(value.c_str());
      printf("  show_ray is: %d\n",show_ray);   
   } 
   /*if(object=="show_laser")
   {
      show_laser=atoi(value.c_str());
      printf("  show_laser is: %d\n",show_laser);   
   } */    
   if(object=="results_time")
   {      
      time_to_show_results=atof(value.c_str());
      printf("  time_to_show_results is: %f\n",time_to_show_results);   
   }
}

void process_config_pair3(string object, string v1, string v2, string v3)
{
   if(object=="hit_color")
   {
       hit_color[0]=atof(v1.c_str());
       hit_color[1]=atof(v2.c_str());
       hit_color[2]=atof(v3.c_str());  
	   printf("  setting hit_color: %f %f %f\n",hit_color[0],hit_color[1],hit_color[2]);
   }
   if(object=="miss_color")
   {
       miss_color[0]=atof(v1.c_str());
       miss_color[1]=atof(v2.c_str());
       miss_color[2]=atof(v3.c_str());  
	   printf("  setting miss_color: %f %f %f\n",miss_color[0],miss_color[1],miss_color[2]);
   }

}

string get_token(char *line, const char *delimiters)
{
   char *t =strtok(line,delimiters);
	  
   if(t==NULL)
   { 
      return "";
   }
   if(strlen(t)==0)
      return "";
         
   if(t[0]=='#')
      return "";

   return t;
}

void process_config_file(string filename)
{
   printf("Loading config: %s\n",filename.c_str());
   FILE *cf=fopen(filename.c_str(),"r");
   
   if(cf==NULL)
   {
      printf("unable to open config file for reading: %s\n",filename.c_str());
      exit(1);   
   }
   
   char line[1000];
   const char delimiters[]=" \t\n\r";
   
   int line_num=0;
   while(feof(cf)==0)
   {
      memset(line,0,1000);
      fgets(line,900,cf);
	  
      string tokens[4];
	  int tokens_found=0;
	  
      while(tokens_found<4)
      {
         string t;

         if(tokens_found==0) t=get_token(line,delimiters);
		 else                t=get_token(NULL,delimiters);
		 
         if(t.length()==0)
		    break;
			
	     tokens[tokens_found]=t;
	     tokens_found++;
      }             
      
	  if(tokens_found==2) process_config_pair(tokens[0],tokens[1]);
	  if(tokens_found==4) process_config_pair3(tokens[0],tokens[1],tokens[2],tokens[3]);      
      
      line_num++;
   }
   printf("  done processing config file\n");
   fclose(cf);
}

void load_target_file(string filename)
{
   printf("Loading target file: %s\n",filename.c_str());
   FILE *tf=fopen(filename.c_str(),"r");
   
   if(tf==NULL)
   {
      printf("unable to open target file for reading: %s\n",filename.c_str());
      exit(1);   
   }
   
   char line[1000];
   const char delimiters[]=" \t\n\r,";
   
   int line_num=0;
   while(feof(tf)==0)
   {
      memset(line,0,1000);
      fgets(line,900,tf);
      //("read: %s\n",line);
                           
      char *t =strtok(line,delimiters);
      if(t==NULL)
      { 
         //printf("no token1 found on line %d\n",line_num);
         continue;
      }
      if(strlen(t)==0)
         continue;
      if(t[0]=='#')
         continue;
      
      int target=atoi(t);
      //printf("loaded: %d\n",target);
      target_vec.push_back(target);
      
      t =strtok(NULL,delimiters);
      if(t==NULL)
      { 
         //printf("no token2 found on line %d\n",line_num);
         continue;
      }
      if(strlen(t)==0)
         continue;
      
      float delay=atof(t);
      //printf("loaded: %f\n",delay);
      delay_vec.push_back(delay);
      
      t =strtok(NULL,delimiters);
      if(t==NULL)
      { 
         //printf("no token3 found on line %d\n",line_num);
         continue;
      }
      if(strlen(t)==0)
         continue;
      
      float angle_offset=atof(t);
      //printf("loaded: %f\n",delay);
      offset_vec.push_back(angle_offset);
      
      line_num++;
   }
   
   printf("  done processing target file. %d targets loaded. %d delays loaded\n",target_vec.size(),delay_vec.size());
   fclose(tf);
 
}

void open_log_files()
{
   string prefix_and_date=log_prefix+get_logfile();
   string summary_name=prefix_and_date+"_summary.csv";
   string detail_name=prefix_and_date+"_detail.csv";
   
   open_summary_log(summary_name);
   open_detail_log(detail_name);
}
   
arVector3 trap_house_front=arVector3(0,0,-54);
//arVector3 trap_house_front=arVector3(0,0,-10);
   
void setup_trap_task(arMasterSlaveFramework& fw, string config_file, int blocknumber)
{ 
   if(fw.getMaster())
   {
      setup_parallel_port();
      set_parallel_port_value(0); 
      our_block=blocknumber;
      set_parallel_port_value(CODE_START_BLOCK_1+our_block);
      ar_usleep(100000); //make sure the pulse is 100ms long
      set_parallel_port_value(0);
      
   }
   
   process_config_file(config_file);
   
   //Perhaps put block start stuff here
   //set_parallel_port_value(0); 
   
   load_target_file(target_file);
      
   if(fw.getMaster())
   {
      open_log_files();  
   }
   
   claySphere=new sphere(1);

   /*arVector3 v1=arVector3(sin(-45*M_PI/180.0f),0,-cos(-45*M_PI/180.0f));
   arVector3 v2=arVector3(sin(-30*M_PI/180.0f),0,-cos(-30*M_PI/180.0f));
   arVector3 v3=arVector3(sin(  0*M_PI/180.0f),0,-cos(  0*M_PI/180.0f));
   arVector3 v4=arVector3(sin( 30*M_PI/180.0f),0,-cos( 30*M_PI/180.0f));
   arVector3 v5=arVector3(sin( 45*M_PI/180.0f),0,-cos( 45*M_PI/180.0f));*/
   
   float v1=-45.0f;
   float v2=-30.0f;
   float v3=0.0f;
   float v4=30.0f;
   float v5=45.0f;
   
   arVector3 up;

   float v6=-37.5f; //new for spring 2017 (3 horizontal trajectories)
   float v7=0.0f;
   float v8=37.5f;
   
   for(int i=0;i<2;i++)
   {  
      if(i==0) up=arVector3(0.0f,0.47,0.0f);
      //if(i==1) up=arVector3(0.0f,0.23,0.0f);
      if(i==1) up=arVector3(0.0f,0.3,0.0f);
	  
      target_launcher_vec.push_back(new target_launcher(trap_house_front+arVector3(-visual_radius*4.0,0,0),v1,up));
      target_launcher_vec.push_back(new target_launcher(trap_house_front+arVector3(-visual_radius*2,0,0),v2,up));
      target_launcher_vec.push_back(new target_launcher(trap_house_front+arVector3( 0,0,0),v3,up));
      target_launcher_vec.push_back(new target_launcher(trap_house_front+arVector3(visual_radius*2.0,0,0),v4,up));
      target_launcher_vec.push_back(new target_launcher(trap_house_front+arVector3(visual_radius*4.0,0,0),v5,up));
   }
   
   for(int i=0;i<2;i++)
   {  
      if(i==0) up=arVector3(0.0f,0.47,0.0f);
      if(i==1) up=arVector3(0.0f,0.3,0.0f);
	  
      target_launcher_vec.push_back(new target_launcher(trap_house_front+arVector3(-visual_radius*3,0,0) ,v6,up));
      target_launcher_vec.push_back(new target_launcher(trap_house_front+arVector3( 0,0,0)               ,v7,up));
      target_launcher_vec.push_back(new target_launcher(trap_house_front+arVector3(visual_radius*3.0,0,0),v8,up));
   }  
   
   //Sound setup
   const arMatrix4 ident;
   dsTransform( "root sound matrix", "root", ident ); 
} 

#define FLOOR_RANGE 300
void draw_floor_grid()
{
   glColor3f(101.0f/255.0f*0.5,67.0f/255.0*0.5,33.0/255.0f*0.5);
   glDisable(GL_LIGHTING);

   glBegin(GL_LINES);
   for(int i=-FLOOR_RANGE;i<FLOOR_RANGE;i=i+6)
   {
      glVertex3f(i,0,-FLOOR_RANGE);   
      glVertex3f(i,0,FLOOR_RANGE);  

      glVertex3f(-FLOOR_RANGE,0,i);   
      glVertex3f(FLOOR_RANGE,0,i);  
   }
   glEnd(); 
   glEnable(GL_LIGHTING);
}

float trap_house_size=10;
float trap_house_half_width=trap_house_size*0.5;

bool is_targeting_sphere_intersecting=false;
arVector3 targeting_sphere_pos;

void draw_trap_house()
{     
   glBegin(GL_QUADS);
      glNormal3f(0,1,0);
      glVertex3f(-trap_house_half_width,0.1f,trap_house_front[2]);   
      glVertex3f( trap_house_half_width,0.1f,trap_house_front[2]);  
      glVertex3f( trap_house_half_width,0.1f,trap_house_front[2]+trap_house_size);   
      glVertex3f(-trap_house_half_width,0.1f,trap_house_front[2]+trap_house_size);  
   glEnd(); 
}

arVector3 cal_pos;
float cal_distance=10000.0f;

bool clear_once=false;


int wait_to_launch=0;

string text_results="";
arVector3 target_pos_results;
void draw_trap_task()
{  

   //if(clear_once==false) //for making trail diagrams
   {
   //if(state!=TARGET_MOVING && state!=SHOT_ATTEMPTED && state!=EXTRA_WAITING && state!=TARGET_OUT_OF_RANGE) //for cool multiple sphere pics
   {
      glClearColor(0.05f,0.05f,0.05f,0.0f); //world background color black
      glClearDepth(1.0);
      glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
   }
      clear_once=true;
   }
   
   
   
   
   

   char buffer[100]; //for text

   if(isWandReady==false) //wait for want to be connected
   {
      char buffer[100];
      sprintf(buffer,"Please Wait...\n");                          render_text_always(buffer,-0.4,0.3);
      return;
   }
   

   if(state==TARGET_ALL_DATA_SAVED)
   {
      char buffer[100];
      sprintf(buffer,"Save Completed");   
      render_text_always(buffer,-0.15,0.0);  
      return;
   }
   

   
   draw_floor_grid();
   
   
   
   
   
   //if(state==TARGET_WAITING && touching_trap_house==false) glColor3f(0.75,0.00,0.00); 
   //else                                                    glColor3f(0.00,0.75,0.00);    
   glDisable(GL_LIGHTING);
    
   if((state==TARGET_WAITING && touching_trap_house==false) || state==CALIBRATION_STATE || state==PRECALIBRATION_STATE || state==PRERESTING_STATE) 
      glColor3f(0.30,0.00,0.00); 
   else
      glColor3f(0.00,0.27,0.00);    
   
   draw_trap_house();
   
    glEnable(GL_LIGHTING);
   
   if(state!=TARGET_WAITING && state!=CALIBRATION_STATE && state!=PRECALIBRATION_STATE && state!=PRERESTING_STATE)
   {
      if(state==TARGET_RESULTS || state==TARGET_ALL_COMPLETE)
      {
         if(is_success)
            glColor3f(hit_color[0],hit_color[1],hit_color[2]);
         else
            glColor3f(miss_color[0],miss_color[1],miss_color[2]);    
      }
      else
      {
         glColor3f(255.0f/255.05,116.0f/255.0f,59.0f/255.0f);
      }
      
	  if(state==TARGET_RESULTS || state==TARGET_ALL_COMPLETE)
	  {
	     glPushMatrix();         
            glTranslatef(target_pos_results.v[0],target_pos_results.v[1],target_pos_results.v[2]);
            glScalef(visual_radius,visual_radius,visual_radius);
            claySphere->draw();
         glPopMatrix();  
		 arVector3 display_pos(0,3,-5);
		 
		 
		 //bonus
	/*	 glPushMatrix();
		    glTranslatef(-target_pos_results.v[0]+display_pos.v[0],-target_pos_results.v[1]+display_pos.v[1],-target_pos_results.v[2]+display_pos.v[2]);
		 
		 glPushMatrix();         
            glTranslatef(target_pos_results.v[0],target_pos_results.v[1],target_pos_results.v[2]);
            glScalef(visual_radius,visual_radius,visual_radius);
            claySphere->draw_outline();
         glPopMatrix();  
		 
		  glColor3f(0.8f,0.8f,0.8f);
    
	
	      
	    glDisable(GL_LIGHTING);

       glPushMatrix();      
         //glTranslatef(targeting_sphere_pos.v[0],targeting_sphere_pos.v[1],targeting_sphere_pos.v[2]);
         //glScalef(0.25f,0.25f,0.25f);
         //claySphere->draw(); 
		 glPointSize(targeting_size);
		 glBegin(GL_POINTS);
		   glVertex3f(targeting_sphere_pos.v[0],
		              targeting_sphere_pos.v[1],
					  targeting_sphere_pos.v[2]);
			glColor3f(0.0f,0.8f,0.8f);		  
			glVertex3f(closest_point.v[0],
		              closest_point.v[1],
					  closest_point.v[2]);
		 glEnd();
      glPopMatrix();        
	  
	     glPushMatrix();
          glBegin(GL_LINES);
            glColor3f(1.0f,0.0,0.0);
            //glVertex3f(0.0f,0.0f,0.0f); //-wand_length*0.5f);
      
	  glVertex3f(wand_position_at_hit.v[0],
		              wand_position_at_hit.v[1],
					  wand_position_at_hit.v[2]);
            glVertex3f(closest_point.v[0],
		              closest_point.v[1],
					  closest_point.v[2]);
         glEnd();
      glPopMatrix();
	  
	  glEnable(GL_LIGHTING);
	  glPopMatrix();
	  */
	  }
	  else
      {
	     glPushMatrix();         
            glTranslatef(target_pos.v[0],target_pos.v[1],target_pos.v[2]);
            glScalef(visual_radius,visual_radius,visual_radius);
      
            //if(state==SHOT_ATTEMPTED && do_shatter_graphics)
            //   claySphere->draw_expanded(explosion_expansion);
            //else
            claySphere->draw();
         glPopMatrix();  
      }
   }
   if(state==TARGET_RESULTS || state==TARGET_ALL_COMPLETE)
   {
      glColor3f(0.8f,0.8f,0.8f);
  	  glPointSize(targeting_size);

	  glDisable(GL_LIGHTING);
	  glPushMatrix();     
         glBegin(GL_POINTS);
	        glVertex3f(targeting_sphere_result.v[0],targeting_sphere_result.v[1],targeting_sphere_result.v[2]);
	     glEnd();
      glPopMatrix();        
	  glEnable(GL_LIGHTING);
   }
   
   if(is_targeting_sphere_intersecting && show_targeting_sphere)
   {
      glColor3f(0.8f,0.8f,0.8f);
    
	  glDisable(GL_LIGHTING);	  
	   
      glPushMatrix();      
         //glTranslatef(targeting_sphere_pos.v[0],targeting_sphere_pos.v[1],targeting_sphere_pos.v[2]);
         //glScalef(0.25f,0.25f,0.25f);
         //claySphere->draw(); //to draw reticle as 3d object instead
		 glPointSize(targeting_size);
		 glBegin(GL_POINTS);
		   glVertex3f(targeting_sphere_pos.v[0],targeting_sphere_pos.v[1],targeting_sphere_pos.v[2]);
		 glEnd();
      glPopMatrix();        
	  glEnable(GL_LIGHTING);

   }
   
   if(show_ray && state==CALIBRATION_STATE)
   {
      glColor4f(1,0,0,0.5); //wand stays red
      duckWand.draw();
   }
  
   sprintf(buffer,"Flight: %0.1fs %04.1fyd",flight_time,(target_pos-launch_pos).magnitude()/3.0f);   render_text(buffer,-0.9,0.9);
   sprintf(buffer,"From user (yd): %04.1f",(target_pos-arVector3(0,1.75,0)).magnitude()/3.0f);       render_text(buffer,-0.9,0.8);
   sprintf(buffer,"Ring Ht.  (yd): %0.1f",ring_height/3.0f);                                        render_text(buffer,-0.9,0.7);
   sprintf(buffer,"Shot1: %0.1fs %04.1f %.2f [%d]",shot_timing[0],shot_distance[0]/3.0f,shot_closeness[0],shot_result[0]);   render_text(buffer,-0.9,0.6);
   sprintf(buffer,"Shot2: %0.1fs %04.1f %.2f [%d]",shot_timing[1],shot_distance[1]/3.0f,shot_closeness[1],shot_result[1]);    render_text(buffer,-0.9,0.5);
   
   
   if(state==CALIBRATION_STATE)
   {
    
	  //else
	  {
         if(cal_distance>=5.0f)
	     {
            char buffer[100];
            sprintf(buffer,"Calibration BAD\n");                          render_text_always(buffer,-0.4,0.3);
            sprintf(buffer,"Left/Right:       %0.0f %s\n",cal_pos[0], cal_pos[0]>0? "(move left)":"(move right)");   render_text_always(buffer,-0.4,0.1);
            sprintf(buffer,"Forward/Backward: %0.0f %s\n",cal_pos[2], cal_pos[2]>0? "(move forward)":"(move back)");   render_text_always(buffer,-0.4,0.0);
            sprintf(buffer,"Total Distance:   %0.0f\n",cal_distance); render_text_always(buffer,-0.4,-0.1);
         }
         else
	     {
	        char buffer[100];
            sprintf(buffer,"Calibration GOOD - Press Trigger\n");                          render_text_always(buffer,-0.4,0.3);
	     }  
	  }
	  return;
   }
   
   if(state==TARGET_RESULTS || state==TARGET_ALL_COMPLETE)
   {
      float top_item=0.5;
      char buffer[100];
      
	  //sprintf(buffer,text_results.c_str());   
      //render_text_always_little_bigger(buffer,-0.8,top_item);    
	  
	  int targets_to_go=target_vec.size()-(current_trial+1);
      sprintf(buffer,"Remaining: %d ",targets_to_go);         
      //render_text_always_little_bigger(buffer,-0.8,top_item-0.08);
	  
	  float dist_ratio=(-target_pos_results.v[2]/200.0f);
	  
	  render_3d_text(buffer,target_pos_results.v[0]-1.0f*dist_ratio,target_pos_results.v[1]+9.0f*dist_ratio,target_pos_results.v[2],0.03f*dist_ratio);
	  
	  float accuracy=(float)number_of_success_shots/(float)(current_trial+1)*100.0f;
	  sprintf(buffer,"Accuracy: %d%%",(int)accuracy);         
      //render_text_always_little_bigger(buffer,-0.8,top_item-(0.08*2.0));
 	  render_3d_text(buffer,target_pos_results.v[0]-1.0f*dist_ratio,target_pos_results.v[1]+4.0f*dist_ratio,target_pos_results.v[2],0.03f*dist_ratio);

   }
   
   if(state==TARGET_ALL_COMPLETE)
   {
      char buffer[100];
      sprintf(buffer,"Please Wait. Saving Data...");   
      render_text_always(buffer,-0.3,0.0);  
      //return;
   }
   if(state==PRERESTING_STATE)
   {
      char buffer[100];
      sprintf(buffer,"Please Press Trigger to Enter Resting State");   
      render_text_always(buffer,-0.45,0.0);  
   }
   
   if(parallel_code!=0) //for LED
   {      
      glColor3f(1,1,1);
      glDisable(GL_LIGHTING);

      float LED_SIZE=0.2;
      
	  float bottom=0.1;
	  float left=4.25;
	  
      glBegin(GL_QUADS);
         glVertex3f(4.75,bottom,left);
         glVertex3f(4.75,bottom+LED_SIZE,left);
         glVertex3f(4.75,bottom+LED_SIZE,left+LED_SIZE);
         glVertex3f(4.75,bottom,left+LED_SIZE);
      glEnd(); 
      
      glEnable(GL_LIGHTING);
   }
   
   
}


bool interpolated_collision_detection(arVector3 oldWPos, arVector3 oldWFor, arVector3 newWPos, arVector3 newWFor, arVector3 oldTPos, arVector3 newTPos, arVector3 &target_position_at_hit, float &target_distance)
{
   bool result=false;
   target_distance=HUGE_VAL;
   
   for(float i=0;i<=1.0;i+=0.05)
   {
      float e=1.0-i;
      
      arVector3 interpTPos=oldTPos*e+newTPos*i; 
      arVector3 interpWPos=oldWPos*e+newWPos*i;
      arVector3 interpWFor=(oldWFor*e+newWFor*i).normalize();
   
      arVector3 potential_closest_point=ar_projectPointToLine(interpWPos,interpWFor,interpTPos);
      arVector3 distance=interpTPos-potential_closest_point;
      float mag=distance.magnitude();
     
      if(mag<target_distance) //is this better then what we've already found?
      {
	     
         target_position_at_hit=interpTPos; 
		 wand_position_at_hit=interpWPos;
         target_distance=mag;
         closest_point=potential_closest_point;
      }
   }
   
   
   return (target_distance<selection_radius); 
}

//http://gamedev.stackexchange.com/questions/96459/fast-ray-sphere-collision-code
arVector3 IntersectRaySphere(arVector3 point, arVector3 dir, arVector3 sphere_center, float sphere_radius) 
{
   arVector3 m = point - sphere_center; 
   float b = m.dot(dir);  
   float c = m.dot(m) - sphere_radius * sphere_radius; 
   float discr = b*b - c; 

   // Ray now found to intersect sphere, compute smallest t value of intersection
   float t = -b - sqrt(discr); 

   return point + t * dir; 
}

void compute_targeting_sphere(arVector3 target_p, arVector3 w_pos, arVector3 w_for)
{
   if(state!=TARGET_MOVING && state!= TARGET_WAITING && state!=TARGET_RESULTS) //only show targeting sphere is specific cases
      return; 
	  
   is_targeting_sphere_intersecting=false;


   float ground_intersection_range;
   bool is_intersection_ground=ar_intersectLinePlane(w_pos,
                                                     w_for,
                                                     arVector3(0,0.1,0), //plane point
                                                     arVector3(0,1,0), //plane normal
                                                     ground_intersection_range);
   
   arVector3 ground_intersection_point=w_pos+ground_intersection_range*w_for;
   if(is_intersection_ground==true && ground_intersection_range<0.0f)
       is_intersection_ground=false;
       
   float target_intersection_range;
   arVector3 plane_point=target_p;
   plane_point[2]+=visual_radius; //bump back to user (so we aren't inside the target sphere)
   
   bool is_intersection_target=false;
   arVector3 target_intersection_point;
   
   if(state==TARGET_MOVING)
   {   
      is_intersection_target=ar_intersectLinePlane(w_pos,
                                                     w_for,
                                                     plane_point, //plane point
                                                     arVector3(0,0,1), //plane normal
                                                     target_intersection_range);
      //printf("target plane distance: %f\n",target_intersection_range);
      target_intersection_point=w_pos+target_intersection_range*w_for;
      if(is_intersection_target==true && target_intersection_range<0.0f)
         is_intersection_target=false;
   }
   if(state==TARGET_WAITING || state==TARGET_RESULTS)
   {
       is_intersection_target=ar_intersectLinePlane(w_pos,
                                                     w_for,
                                                     trap_house_front, //front of trap house
                                                     arVector3(0,0,1), //plane normal
                                                     target_intersection_range);
      //printf("target plane distance: %f\n",target_intersection_range);
      target_intersection_point=w_pos+target_intersection_range*w_for;
      if(is_intersection_target==true && target_intersection_range<0.0f)
         is_intersection_target=false;
   
   
   }
   
   if(is_intersection_target==false && is_intersection_ground==false) //no intersections
   {
       is_targeting_sphere_intersecting=false;
   }
   if(is_intersection_target==true && is_intersection_ground==false) //just intersecting target plane
   { 
      //printf("touching target plane");
      is_targeting_sphere_intersecting=true;
      targeting_sphere_pos=target_intersection_point;   
   }
   if(is_intersection_target==false && is_intersection_ground==true) //just intersecting ground plane
   {    
      //printf("touching ground plane");
      is_targeting_sphere_intersecting=true;
      targeting_sphere_pos=ground_intersection_point;
   }           
   
   if(is_intersection_target==true && is_intersection_ground==true) //touching both? pick closest
   {   
      //printf("touching both");
	  is_targeting_sphere_intersecting=true;

      if(target_intersection_range<ground_intersection_range)
         targeting_sphere_pos=target_intersection_point;   
      else
         targeting_sphere_pos=ground_intersection_point;
   }
}




bool is_user_aiming_at_trap_house()
{

   // lineDirection & planeNormal need not be normalized (but affects interpretation of range).
   // Returns false if no intersection.
   // Otherwise, intersection = linePoint + range*lineDirection.
   float intersection_range;
   bool is_intersection_ground=ar_intersectLinePlane(duckWand.get_position(),
                                                     duckWand.get_forward(),
                                                     arVector3(0,0.1,0), //plane point
                                                     arVector3(0,1,0), //plane normal
                                                     intersection_range);
   if(is_intersection_ground==false)
       return false;
       
   arVector3 intersection_point=duckWand.get_position()+intersection_range*duckWand.get_forward();
   
   float x=intersection_point[0];
   float z=intersection_point[2];
   
   if(z<trap_house_front[2]) return false;
   if(z>(trap_house_front[2]+trap_house_size)) return false;
   if(x>trap_house_half_width) return false;
   if(x<-trap_house_half_width) return false;
   
   return true;    
}

float time_aimed_at_trap_house=0;
int target_complete=0;
arMatrix4 prev_matrix;

float compute_matrix_diff(arMatrix4 a, arMatrix4 b)
{
   float matrix_diff=0.0f;
   
   for(int i=0;i<16;i++)
   {
      matrix_diff+=fabs(a[i]-b[i]);   
   }
   
   return matrix_diff;
}

void send_block_done()
{
   if(block_done_sent==false)
   {
      set_parallel_port_value(CODE_END_BLOCK_1+our_block);
      ar_usleep(100000); //make sure the pulse is 100ms long
      set_parallel_port_value(0);
      block_done_sent=true;
   }
}

float results_shown_time=0.0f;

int bonus_frames=0;

void update_trap_task(arMasterSlaveFramework& fw, double t)
{ 
    //arVector3 hp=ar_extractTranslation(fw.getMatrix(2));
    //printf("axis: %.02f %.02f %.02f\n", fw.getAxis(2),fw.getAxis(3),fw.getAxis(4));
    //printf("gun: %.02f %.02f %.02f\n",hp.v[0],hp.v[1],hp.v[2]); //to calibrate gun

   parallel_code=0;
   
   /*if(state==TARGET_RESULTS)
   {
      results_shown_time+=t;
      if(results_shown_time>time_to_show_results)
      {      
         state=TARGET_WAITING;     
         results_shown_time=0.0f;
      }
	  return;
   }*/
 
   
   if(state==TARGET_ALL_COMPLETE)
   {
      target_complete++;
      
      if(target_complete==30) //wait a bit, so we are sure the "all complete" message gets displayed to the user. 
      {
         if(fw.getMaster())
         {  
            dump_summary_log();
            dump_detail_log();
            send_block_done();
            
            dsLoop("trigger sound success", "root sound matrix", sound_prefix+"done.wav",-1, 1.0, arVector3(0,0,0));
         }
         state=TARGET_ALL_DATA_SAVED;
      }
      return;
   }
   
   if(state==TARGET_ALL_DATA_SAVED)
      return;   

   if(do_gun==0)
      button=fw.getButton(5);
   else 
      button=fw.getAxis(4)<-10.0f;
	  
   //bool button2=fw.getButton(2); 
   //bool button3=fw.getButton(3);
   //bool buttonX=fw.getButton(4);// | fw.getButton(8); 
   
   //int num_wands=fw.getNumberMatrices()-1; //this may not be accurate
   
   arVector3 prevPos=duckWand.get_position();
   arVector3 prevFor=duckWand.get_forward();
   
   //if(num_wands>0)
   {
      duckHead.update(fw.getMatrix(0));
	  if(do_gun==false)
        duckWand.update(fw.getMatrix(1),false);
	  else
	    duckWand.update(fw.getMatrix(2),true);
   }         
 
   arVector3 newPos=duckWand.get_position();
   arVector3 newFor=duckWand.get_forward();    
   //cout << " wand pos: " << newPos << endl;
   
   if(isWandReady==false)
   {
      float sanity_check=fabs(newPos[0])+fabs(newPos[1])+fabs(newPos[2]);
	  if(sanity_check!=0.0f)
	     isWandReady=true;
   }  

   if(state==PRERESTING_STATE)
   {
      if(button==true && button_previous==false && isWandReady)
	   {
	     state=PRECALIBRATION_STATE;
        if(fw.getMaster()) 
           set_parallel_port_value(CODE_REST_START);
        prev_parallel_code=parallel_code;
	   }
      button_previous=button;
	   return;
   }
   if(state==PRECALIBRATION_STATE)
   {
      if(button==true && button_previous==false && isWandReady)
	   {
	     state=CALIBRATION_STATE;
        if(fw.getMaster())
           set_parallel_port_value(CODE_REST_END);
        prev_parallel_code=parallel_code;
	   }
      button_previous=button;
	   return;
   }      
   
   if(state==CALIBRATION_STATE)
   {
      cal_pos=newPos*30.48; //get to cm
      cal_pos[1]=0.0f; //zero out the y term
      
      cal_distance=cal_pos.magnitude(); //get to cm
      	  
      if(cal_distance<5.0f && button && button_previous==false) // && ((newPos[0]==0.0f && newPos[1]==0.0f && newPos[2]==0.0f) == false) ) 
         state=TARGET_WAITING;

      button_previous=button;
	
      return;
   } 
   
   if(state==TARGET_RESULTS)
   {
      compute_targeting_sphere(target_pos, duckWand.get_position(), duckWand.get_forward());
	 	  
	  bonus_frames++;
      if(bonus_frames==10)
      {
         parallel_code=CODE_ENTERED_WAIT_FOR_LAUNCH;   
	  }
	  
	  if(bonus_frames>20)
	  {
         if(current_trial==(target_vec.size()-1))
            state=TARGET_ALL_COMPLETE;
         else if(is_user_aiming_at_trap_house())
            state=TARGET_WAITING;	   
      }
   }
   
   if(state==TARGET_WAITING)
   {  
      compute_targeting_sphere(target_pos, duckWand.get_position(), duckWand.get_forward());

   
      closest_point=arVector3(-1,-1,-1);      
      how_far_from_target=-1;
      target_angular_size=-1;
      current_shot_distance=-1;
      
      if(is_user_aiming_at_trap_house())
      {       
         if(time_aimed_at_trap_house==0.0f)
         {
            parallel_code=CODE_BEGIN_TRAP_TOUCH;
         }
         
         time_aimed_at_trap_house+=t;
         if(time_aimed_at_trap_house>0.5)
         {
            
            if(touching_trap_house==false)
            {
               parallel_code=CODE_TRIAL_PRELAUNCH_1+current_trial+1;
               //printf("time aimed at trap house: %f\n",time_aimed_at_trap_house);
            }
            
            touching_trap_house=true;
         }
      }
      else
      {
         if(time_aimed_at_trap_house!=0.0f)
            parallel_code=CODE_STOP_TRAP_TOUGH;
      
         time_aimed_at_trap_house=0.0f;
         touching_trap_house=false;
      }
      
     // arMatrix4 wand_matrix=fw.getMatrix(1);         
 
      wait_to_launch++;
      
      //if(wait_to_launch>50) //to auto launch
      if(time_aimed_at_trap_house>(delay_vec[current_trial+1])) // && (compute_matrix_diff(wand_matrix,prev_matrix)!=0.0f)) || (buttonX==true))
      //if(buttonX==true)
      {              
      
         printf("Launching clay!\n");
         //printf("  time aimed at trap house: %f\n",time_aimed_at_trap_house);
         //float rand_val;
         //fw.randUniformFloat(rand_val);
         
         current_trial++;
		 if(sizeafter6!=-1.0f && current_trial>5) //hack to 
		 {
		    visual_radius=sizeafter6;
			selection_radius=sizeafter6;		 
		 }
                  
         target_slot=target_vec[current_trial];
         angle_offset=offset_vec[current_trial];
         if(target_slot>=target_launcher_vec.size())
         {
            printf("ERROR: target slot %d out of range\n",target_slot);
            target_slot=target_launcher_vec.size()-1;
         }  
         
         trial_begin=fw.getTime()/1000.0f; //get in seconds
         printf("launching with angle offset: %f\n",angle_offset);
         target_launcher_vec[target_slot]->launch(angle_offset);
         target_past_ring=false;
         local_speed=target_speed;
         flight_time=0;
         flight_time_method2=0;
         explosion_expansion=0;
         
         time_aimed_at_trap_house=0.0f;
         touching_trap_house=false;
         
         shot_timing[0]=-1;
         shot_timing[1]=-1;
         shot_distance[0]=-1;
         shot_distance[1]=-1;
         shot_closeness[0]=-1;
         shot_closeness[1]=-1;
         shot_result[0]=-1;
         shot_result[1]=-1;
         shot_angular_size[0]=-1;
         shot_angular_size[1]=-1;
         
         out_of_range=false;
         is_success=false;
         
         if(fw.getMaster())
		   {
		      dsLoop("trigger sound launch", "root sound matrix", sound_prefix+"launch.wav",-1, 1.0, arVector3(0,0,0));
        	}
         
         state=TARGET_MOVING;
         shots_taken=0;
         
         parallel_code=CODE_LAUNCH_0+target_slot;
      }
      
      //prev_matrix=wand_matrix;
   }   
   
   arVector3 prevTargetPos=target_pos;
   arVector3 target_shot_pos;
		 
   if(state==TARGET_MOVING)
	{
      float lift_coeff=0.0100; 
      float air_drag=  0.0010;
      
      local_speed-=air_drag*local_speed*local_speed*t; //apply drag to forward movement?
      //printf("localspeed: %f\n",local_speed);
      target_pos+=target_vector*t*local_speed; //move target
      downward_velocity+=gravity*t;
      downward_velocity-=lift_coeff*downward_velocity*downward_velocity*t; //apply lift force

      //printf("downward velocity: %f\n",downward_velocity);
      target_pos[1]-=downward_velocity*t;      
      //printf("target_pos[1]: %f\n",target_pos[1]);
      
      float target_ring=27;
      
      arVector3 temp_pos=arVector3(target_pos[0],0,target_pos[2]);
      float dist_on_ground=(temp_pos-launch_pos).magnitude();
      
      //printf("downward velocity: %f\n",downward_velocity);
      
      if(dist_on_ground>target_ring && target_past_ring==false)
      {
         //printf("target height at 9yds (%f::%f) in front of first sight=%f\n",dist_on_ground,target_ring,target_pos[1]); 
         ring_height=target_pos[1];         
         target_past_ring=true;         
      }
      flight_time+=t;
      flight_time_method2=(fw.getTime()/1000.0f)-trial_begin;
      
      arVector3 newTargetPos=target_pos;
	  
      compute_targeting_sphere(target_pos, duckWand.get_position(), duckWand.get_forward());
   
      bool is_hit=interpolated_collision_detection(prevPos,prevFor,newPos,newFor,prevTargetPos,newTargetPos,target_shot_pos,how_far_from_target);
      
	   if((button==true && button_previous==false) ) 
	   {
         
         shot_closeness[shots_taken]=how_far_from_target;
         printf("is hit: %d %f %f\n",is_hit,how_far_from_target,selection_radius);
         
         shot_timing[shots_taken]=flight_time;
         shot_distance[shots_taken]=current_shot_distance;
         shot_angular_size[shots_taken]=target_angular_size;
                 
		 shot_result[shots_taken]=1;

		 //targeting_sphere_pos=closest_point; //show hanging target sphere at closest point 
		 target_pos_results=target_shot_pos;
		 
         if(is_hit) // && state!=TARGET_OUT_OF_RANGE)
         {
            printf("You got it!\n");
            
            is_success=true;
            //shot_result[shots_taken]=1;
            if(fw.getMaster())
		      {
               if(do_shatter_sound)
                  dsLoop("trigger sound success", "root sound matrix", sound_prefix+"clay_shatter.wav",-1, 1.0, arVector3(0,0,0));
               else
                  dsLoop("trigger sound fail", "root sound matrix", sound_prefix+"trigger.wav",-1, 0.25, arVector3(0,0,0));
        	   }
            if(shots_taken==0)
               parallel_code=CODE_FIRST_HIT;
            if(shots_taken==1)
               parallel_code=CODE_SECOND_HIT;
               
            text_results="Hit";
			number_of_success_shots++;
            targeting_sphere_result=IntersectRaySphere(wand_position_at_hit, (closest_point-wand_position_at_hit).normalize(), target_pos_results, selection_radius);

         }
         else	
         { 
		      if(fw.getMaster())
		      {
		         dsLoop("trigger sound fail", "root sound matrix", sound_prefix+"trigger.wav",-1, 0.25, arVector3(0,0,0));
            }
            //if(state==TARGET_OUT_OF_RANGE)
            //   shot_result[shots_taken]=2;
            //else
            {
               //shot_result[shots_taken]=1;
               
               if(shots_taken==0)
                  parallel_code=CODE_FIRST_MISS;
               if(shots_taken==1)
                  parallel_code=CODE_SECOND_MISS;
            }             
            
            text_results="Miss";
            shots_taken++;
		    compute_targeting_sphere(target_pos_results, wand_position_at_hit, (closest_point-wand_position_at_hit).normalize());
            targeting_sphere_result=targeting_sphere_pos;
         }
         state=TARGET_RESULTS;
		 bonus_frames=0;
      }
      else if(target_pos[1]<=visual_radius && flight_time>1.0f) //if target touching the ground?
      { 
         float seconds_so_far=(fw.getTime()/1000.0f)-trial_begin;
         printf("target hit ground at at: %f\n",seconds_so_far);
               
         parallel_code=CODE_NO_SHOT_TAKEN;
		 target_pos_results=target_pos;
		 wand_position_at_hit=duckWand.get_position();
         text_results="No Shot";
         state=TARGET_RESULTS;     
		 bonus_frames=0;
         targeting_sphere_result=targeting_sphere_pos;		 
      }
      
      if(is_hit) current_shot_distance=(target_shot_pos-arVector3(0,1.75,0)).magnitude(); //TODO base off of actual head pos?
      else       current_shot_distance=(target_pos-arVector3(0,1.75,0)).magnitude();
         
      target_angular_size=fabs(atan(visual_radius/current_shot_distance)*2.0f*180.0f/M_PI);     
      	  
   }
						 
					  
   if(state==TARGET_RESULTS && prev_state!=TARGET_RESULTS && prev_state!=CALIBRATION_STATE) //this is where the going beyond 0 (to negative shot numbers) bug was whoops! used to be TARGET_WAITING
   {
      if(fw.getMaster())
         log_trial_summary();
      
      time_aimed_at_trap_house=0;
      
   }         
   
   if(state==TARGET_WAITING && prev_state!=TARGET_WAITING)
   {
      //parallel_code=CODE_ENTERED_WAIT_FOR_LAUNCH;   
   }
            
   log_trial_detail(fw.getTime());
            
   prev_state=state;
   button_previous=button;
   //button2_previous=button2;
   //button3_previous=button3;
   //buttonX_previous=buttonX;
   
   if(fw.getMaster() && parallel_code!=prev_parallel_code) //only send code if its changed
   {
      set_parallel_port_value(parallel_code);
   }
   
   prev_parallel_code=parallel_code;
}

void cleanup_trap_task(arMasterSlaveFramework& fw)
{
   if(fw.getMaster())
   {
      dump_summary_log(); 
      dump_detail_log();    
      send_block_done();
   }
}
