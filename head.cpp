#include "head.h"
#include <GL/gl.h>
#include "opengl_shapes.h"
#include "arGlut.h"

head::head()
{
   
}

void head::update(arMatrix4 m4)
{   
   _raw=m4;
   //_raw_fixed=ar_matrixToNavCoords(_raw); //get heads position in world coordinates if using travel
   _pos=ar_extractTranslation(_raw);
   _forward=get_forward();
}

arVector3 head::get_position()
{ 
   return _pos;
}

arMatrix4 head::get_matrix()
{
   return _raw;
}

arVector3 head::get_forward()
{
   return _raw*arVector3(0,0,-1) - _raw*arVector3(0,0,0);
}

arVector3 head::get_up()
{
   return _raw*arVector3(0,1,0) - _raw*arVector3(0,0,0);
}

