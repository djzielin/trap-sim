#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/wglew.h>
#include <stdio.h>
#include <stdlib.h>

#include "frame_buffer.h"

void frame_buffer::bind()
{
   if(isSetup==false)
   {
      initialize();
      isSetup=true;   
   }
	glBindFramebuffer( GL_DRAW_FRAMEBUFFER, frameBuffer);
   
  // GLuint attachments[1] = { GL_COLOR_ATTACHMENT0};
//glDrawBuffers(1,  attachments);
}

void frame_buffer::unbind()
{
	glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0);
}

void frame_buffer::draw()
{
	//glBlendFunc  (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	//glEnable     (GL_BLEND);
	//glEnable     (GL_COLOR_MATERIAL);
	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureName);

	//glViewport(0,0,x,y);
			
   glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0,1,0,1, -1.0, 1.0);	
	glMatrixMode   (GL_MODELVIEW);
	glLoadIdentity();

	// draw textured quad
   //glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);
		glTexCoord2f(0, 0);glVertex2f(0,0);
		glTexCoord2f(1, 0);glVertex2f(1,0);
		glTexCoord2f(1, 1);glVertex2f(1,1);
		glTexCoord2f(0, 1);glVertex2f(0,1);
	glEnd();
   
  // glEnable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

}

void frame_buffer::initialize()
{
   printf("in frame_buffer::initialize()\n");
	glGenTextures(1, &textureName);
	glBindTexture(GL_TEXTURE_2D, textureName);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //GL_NEAREST
   glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, x, y, 0, GL_BGR, GL_UNSIGNED_BYTE, NULL); //alloc memory on card  //used to be RGBA
      
	glGenFramebuffers(1, &frameBuffer); //create the frame buffer
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);     
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureName, 0); //attach texture to frame buffer
   
   glGenRenderbuffers(1, &depth_rb);
   glBindRenderbuffer(GL_RENDERBUFFER, depth_rb);
   glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, x, y);
   glBindRenderbuffer(GL_RENDERBUFFER, 0);
   
   glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_rb);
   
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER); //error checking
	if(status!=GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Error Setting up the Frame Buffer!!\n");
      exit(1);
	}
   else 
      printf("   SUCCESS!\n");
      
   glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D,0);
}