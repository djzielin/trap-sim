/* ML2VR. David J. Zielinski. djzielin@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//This file has primitives for cube, pyramid, and sphere

/*****************************************************************/
/* cube.cpp - originally from William R. Sherman's shapes.c file */
/*                                                               */
/* djzielin 3-9-2007                                             */
/* added display lists into this file: 4-15-2010                 */
/*****************************************************************/
#ifdef USE_SZG
   #include "arPrecompiled.h"
#endif

#include "arMasterSlaveFramework.h"

#include "opengl_shapes.h"

GLuint cdl_w_handle;
GLuint cdl_handle;

void init_display_list_cube()
{
   cdl_w_handle =glGenLists(1);
   printf("GL_cube - wireframe cube has display list handle: %d\n",cdl_w_handle);

   glNewList(cdl_w_handle,GL_COMPILE);
      GL_draw_cube_wireframe();
   glEndList();

   cdl_handle =glGenLists(1);
   printf("GL_cube - cube has display list handle: %d\n",cdl_handle);
   glNewList(cdl_handle,GL_COMPILE);
      GL_draw_cube();
   glEndList();
}

void draw_display_list_cube()
{
   glCallList(cdl_handle);
}

void draw_display_list_wireframe_cube()
{
   glCallList(cdl_w_handle);
}

/*****************************************************************/
/*
   Cube (and pyramid) vertices.  Labeled 0-7 (cube) & 8 (top of pyramid).


             7_______ 4
             /|     /|        (+y)
            / | .8 / |         |
          0/__|___/3 |         |
           | 6|___|__|5        o---- (+x)
           | /    | /         /
           |/     |/         /
          1|______|2       (+z)

*/


float cube_vertices[9][3] = {			/* specifies the (x,y,z)   */
   { -1.0,  1.0,  1.0 },	/* 0 */	/* location of each vertex */
   { -1.0, -1.0,  1.0 },	/* 1 */
   {  1.0, -1.0,  1.0 },	/* 2 */
   {  1.0,  1.0,  1.0 },	/* 3 */
   {  1.0,  1.0, -1.0 },	/* 4 */
   {  1.0, -1.0, -1.0 },	/* 5 */
   { -1.0, -1.0, -1.0 },	/* 6 */
   { -1.0,  1.0, -1.0 },	/* 7 */
   {  0.0,  1.0,  0.0 },	/* 8 */
};

int cube_polygons[6][4] = {			/* specifices the vertices */
   { 0, 1, 2, 3 },	/* front  */	/* of each polygon         */
   { 3, 2, 5, 4 },	/* right  */
   { 4, 5, 6, 7 },	/* back   */  //{ 5, 6, 7, 4 },	wrong keep with other orientations
   { 7, 6, 1, 0 },	/* left   */  
   { 6, 5, 2, 1 },	/* bottom */
   { 0, 3, 4, 7 },	/* top    */
};

float cube_tex_coords[4][2] = {
   {1.0, 1.0},
   {1.0, 0.0},
   {0.0, 0.0},
   {0.0, 1.0}
};

int cube_line_index[16] = {0,1,2,3,0,7,6,1,2,5,6,7,4,3,4,5};

float cube_normals[6][3] = {			/* specifies the normal */
   {  0.0,  0.0,  1.0 }, /* front  ** of each polygon      */
   {  1.0,  0.0,  0.0 }, /* right  */
   {  0.0,  0.0, -1.0 }, /* back   */
   { -1.0,  0.0,  0.0 }, /* left   */
   {  0.0, -1.0,  0.0 }, /* bottom */
   {  0.0,  1.0,  0.0 }, /* top    */
};

void GL_draw_cube()
{
   for (int poly = 0; poly < 6; poly++)
   {
	  glBegin(GL_POLYGON);
         glNormal3fv(cube_normals[poly]);
         glTexCoord2f(0.0, 1.0);
         glVertex3fv(cube_vertices[cube_polygons[poly][0]]);
         glTexCoord2f(0.0, 0.0);
         glVertex3fv(cube_vertices[cube_polygons[poly][1]]);
         glTexCoord2f(1.0, 0.0);
         glVertex3fv(cube_vertices[cube_polygons[poly][2]]);
         glTexCoord2f(1.0, 1.0);
         glVertex3fv(cube_vertices[cube_polygons[poly][3]]);
      glEnd();
   }
}

void GL_draw_cube_wireframe()
{
   glNormal3f( 0.0, 1.0,  0.0); 
   glBegin(GL_LINE_STRIP);
      glVertex3fv(cube_vertices[0]);
      glVertex3fv(cube_vertices[1]);
      glVertex3fv(cube_vertices[2]);   
      glVertex3fv(cube_vertices[3]);
      glVertex3fv(cube_vertices[0]);
      glVertex3fv(cube_vertices[7]);
      glVertex3fv(cube_vertices[6]);
      glVertex3fv(cube_vertices[1]);
      glVertex3fv(cube_vertices[2]);
      glVertex3fv(cube_vertices[5]);
      glVertex3fv(cube_vertices[6]);
      glVertex3fv(cube_vertices[7]);
      glVertex3fv(cube_vertices[4]);
      glVertex3fv(cube_vertices[3]);
      glVertex3fv(cube_vertices[4]);
      glVertex3fv(cube_vertices[5]);       
   glEnd();
}

/*****************************************************************/
/* GL_pyramid.cpp - originally from William R. Sherman's shapes.c file */
/*                                                               */
/* djzielin 3-9-2007                                             */
/* added display lists into this file: 4-15-2010                 */
// set up pyramid so it would be for wand (ie, point is in +z direction
/*****************************************************************/

GLuint pdl_w_handle;
GLuint pdl_handle;

void init_display_list_pyramid()
{
   pdl_w_handle =glGenLists(1);
   printf("GL_pyramid - wireframe pyramid has display list handle: %d\n",pdl_w_handle);

   glNewList(pdl_w_handle,GL_COMPILE);
      GL_draw_pyramid_wireframe();
   glEndList();

   pdl_handle =glGenLists(1);
   printf("GL_pyramid - pyramid has display list handle: %d\n",pdl_handle);
   glNewList(pdl_handle,GL_COMPILE);
      GL_draw_pyramid();
   glEndList();
}

void draw_display_list_pyramid()
{
   glCallList(pdl_handle);
}

void draw_display_list_wireframe_pyramid()
{
   glCallList(pdl_w_handle);
}

float py_vertices[9][3] = {			/* specifies the (x,y,z)   */
   { -1.0,  1.0,  1.0 },	/* 0 */	/* location of each vertex */
   { -1.0, -1.0,  1.0 },	/* 1 */
   {  1.0, -1.0,  1.0 },	/* 2 */
   {  1.0,  1.0,  1.0 },	/* 3 */
   {  0.0,  0.0,  -1.0 },	/* 8 */
};

void GL_draw_pyramid() // todo: need texture coordinates and also normals. 
{
      glBegin(GL_TRIANGLES);
        glNormal3f( 0.0, 0.0,  1.0);
        glVertex3fv(py_vertices[2]); //base
        glVertex3fv(py_vertices[1]);
        glVertex3fv(py_vertices[0]);
        glVertex3fv(py_vertices[0]);
        glVertex3fv(py_vertices[3]);
        glVertex3fv(py_vertices[2]);
       

        glNormal3f( -0.894427, 0, -0.447214 );
        glVertex3fv(py_vertices[0]);
        glVertex3fv(py_vertices[1]);
        glVertex3fv(py_vertices[4]);

        glNormal3f( 0, -0.894427, -0.447214 );
        glVertex3fv(py_vertices[1]);
        glVertex3fv(py_vertices[2]);
        glVertex3fv(py_vertices[4]);

        glNormal3f( 0.894427, 0, -0.447214 );
        glVertex3fv(py_vertices[2]);
        glVertex3fv(py_vertices[3]);
        glVertex3fv(py_vertices[4]);


        glNormal3f( 0, 0.894427, -0.447214 );
        glVertex3fv(py_vertices[3]);
        glVertex3fv(py_vertices[0]);
        glVertex3fv(py_vertices[4]);
      glEnd();
   
}

void GL_draw_pyramid_wireframe()
{
   glNormal3f( 0.0, 1.0,  0.0); 
   glBegin(GL_LINES);
      glVertex3fv(py_vertices[0]);
      glVertex3fv(py_vertices[1]);
      glVertex3fv(py_vertices[1]);
      glVertex3fv(py_vertices[2]);
      glVertex3fv(py_vertices[2]);
      glVertex3fv(py_vertices[3]);
      glVertex3fv(py_vertices[3]);
      glVertex3fv(py_vertices[0]);
      glVertex3fv(py_vertices[0]);
      glVertex3fv(py_vertices[4]);
      glVertex3fv(py_vertices[1]);
      glVertex3fv(py_vertices[4]);
      glVertex3fv(py_vertices[2]);
      glVertex3fv(py_vertices[4]);
      glVertex3fv(py_vertices[3]);
      glVertex3fv(py_vertices[4]); 
   glEnd();
}



/***************************************************************************/
/* sphere.cpp - openGL functions to render spheres. method from the rb     */
/* (red book). see page 86 of v1.2.                                        */
/*                                                                         */
/* djzielin 4-19-2004                                                      */
/* added display lists into this				           */
/***************************************************************************/
// precompiled header include MUST appear as the first non-comment line


#include <GL/gl.h>
#include <math.h>
#include <stdio.h>

// float 3 util functions
void copy_GLfloat3(GLfloat *dest,const GLfloat *src);
void copy_GLfloat3_double(GLfloat *dest,const double *src);
void store_GLfloat3(GLfloat a, GLfloat b, GLfloat c, GLfloat *dest);
void normalize_GLfloat3(GLfloat *v);
int  is_GLfloat3_equal(const GLfloat *a, const GLfloat *b);
void calc_GLfloat3_average(const GLfloat *a, const GLfloat *b, GLfloat *ret);
void calc_GLfloat3_average3(const GLfloat *a, const GLfloat *b, const GLfloat *c, GLfloat *ret);
void print_GLfloat3(const GLfloat *v);

#define X .525731112119133606
#define Z .850650808352039932
#define MALLOC malloc

static GLfloat vdata[12][3] = {
  {-X, 0.0, Z}, {X, 0.0, Z},  {-X, 0.0, -Z}, {X, 0.0, -Z},
  {0.0, Z, X},  {0.0, Z, -X}, {0.0, -Z, X},  {0.0, -Z, -X},
  {Z, X, 0.0},  {-Z, X, 0.0}, {Z, -X, 0.0},  {-Z, -X, 0.0}
}; 

static GLuint tdata[20][3]= {
  {1,4,0},  {4,9,0},  {4,5,9},  {8,5,4},  {1,8,4},
  {1,10,8}, {10,3,8}, {8,3,5},  {3,2,5},  {3,7,2},
  {3,10,7}, {10,6,7}, {6,11,7}, {6,0,11}, {6,1,0},
  {10,1,6}, {11,0,9}, {2,11,9}, {5,2,9},  {11,2,7}
};  

void sphere::alloc_memory_for_vertices()
{
   //printf("GL_sphere - allocing memory...\n "); fflush(stdout);

   _face_count = 20*(int)pow(4,_factor);
   _vert_count = (_face_count)*3;    //three vert per face
   _current_pos=0;
   _line_vert_count = _vert_count*2; //begin and end of every vertex

   unsigned int array_size_bytes=sizeof(GLfloat) * _vert_count * 3;  //three positions per vert
   //unsigned int face_normal_size=sizeof(GLfloat) * _vert_count;  //  Need to /3 size 3 verts per face 
   unsigned int line_array_size_bytes=sizeof(GLfloat) *_line_vert_count * 3;  //three positions per vert
  
   //printf("GL_sphere - vert_array: %d line_array: %d\n", array_size_bytes,line_array_size_bytes);

   _vertices=(GLfloat *)MALLOC(array_size_bytes);
   _line_vertices=(GLfloat *)MALLOC(line_array_size_bytes);
   _face_normals=(GLfloat *)MALLOC(array_size_bytes);
   
   if(_vertices==NULL || _line_vertices==NULL || _face_normals==NULL)
   {
      printf("GL_sphere - unable to allocate enough memory for sphere!\n");
      exit(1);
   }

}

//helper function for stack
void sphere::push_vertex(GLfloat *v)
{
   unsigned int push_base=_current_pos*3;
   if((push_base+2) >= (_vert_count*3))
   {
      printf("GL_sphere - error, pushing too many vertices onto stack!\n");
      exit(1);
   }

   _vertices[push_base+0]=v[0];
   _vertices[push_base+1]=v[1];
   _vertices[push_base+2]=v[2];

   _current_pos++;
}

//helper function for stack
void sphere::pop_vertex(float *ret)
{
   int pop_base=(_current_pos-1)*3;
   if(pop_base<0)
   {
      printf("GL_sphere - error pop'd too many vertices off stack!\n");
      exit(1);
   }

   ret[0]=_vertices[pop_base+0];
   ret[1]=_vertices[pop_base+1];
   ret[2]=_vertices[pop_base+2];

   _current_pos--;
}

/***************************************************************************/
/* the recursive subdivision code. pop off the top triangle, and then      */
/* add 4 more                                                              */
/***************************************************************************/
void sphere::add_subdivided(int current_level, int end_level)
{
   if(current_level>end_level) //end the recursion
      return;

   GLfloat a[3]; GLfloat b[3]; GLfloat c[3];

   //pull 4 vertices off the stack
   pop_vertex(c);
   pop_vertex(b);
   pop_vertex(a);

   /*
   //            C
   //           /\
   //          /  \
   //left_avg /____\ right_avg
   //        /\    /\
   //       /  \  /  \
   //      /----\/----\
   //     A   bot_avg  B
   */

   GLfloat bot_avg[3];
   calc_GLfloat3_average(a,b,bot_avg);
   normalize_GLfloat3(bot_avg);

   GLfloat right_avg[3];
   calc_GLfloat3_average(b,c,right_avg);
   normalize_GLfloat3(right_avg);

   GLfloat left_avg[3];
   calc_GLfloat3_average(a,c,left_avg);
   normalize_GLfloat3(left_avg);
  
   //BOTTOM LEFT TRIANGLE
   push_vertex(a); 
   push_vertex(bot_avg);
   push_vertex(left_avg);
   add_subdivided(current_level+1,end_level);
 
   //BOTTOM RIGHT TRIANGLE
   push_vertex(bot_avg);
   push_vertex(b);
   push_vertex(right_avg);
   add_subdivided(current_level+1,end_level);

   //TOP TRIANGLE
   push_vertex(right_avg);
   push_vertex(c);
   push_vertex(left_avg);
   add_subdivided(current_level+1,end_level);

   //CENTER TRIANGLE
   push_vertex(bot_avg);
   push_vertex(right_avg);
   push_vertex(left_avg);
   add_subdivided(current_level+1,end_level);
}

/***************************************************************************/
/* add one of 20 original triangles to top of stack                        */
/***************************************************************************/
void sphere::add_original_triangle(int which_triangle, int end_level)
{
   push_vertex(vdata[tdata[which_triangle][0]]);
   push_vertex(vdata[tdata[which_triangle][1]]);
   push_vertex(vdata[tdata[which_triangle][2]]);

   add_subdivided(1, end_level); //start recursion
}

void sphere::calc_sphere_lines()
{
   //printf("GL_sphere - calcing sphere lines... \n"); fflush(stdout);

   for(unsigned int i=0;i<_vert_count;i+=3)
   {
      copy_GLfloat3(&_line_vertices[(i*2+0)*3],&_vertices[(i+0)*3]); //A to B
      copy_GLfloat3(&_line_vertices[(i*2+1)*3],&_vertices[(i+1)*3]);

      copy_GLfloat3(&_line_vertices[(i*2+2)*3],&_vertices[(i+1)*3]); //B to C
      copy_GLfloat3(&_line_vertices[(i*2+3)*3],&_vertices[(i+2)*3]);

      copy_GLfloat3(&_line_vertices[(i*2+4)*3],&_vertices[(i+1)*3]); //C to A
      copy_GLfloat3(&_line_vertices[(i*2+5)*3],&_vertices[(i+0)*3]);
   }
}

void sphere::calc_face_normals()
{
   for(unsigned int i=0;i<_vert_count*3;i+=9)
   {
      calc_GLfloat3_average3(&_vertices[i],&_vertices[i+3],&_vertices[i+6],&_face_normals[i]); 
      
      arVector3 v=arVector3(_face_normals[i],_face_normals[i+1],_face_normals[i+2]);
      arVector3 v2=v.normalize();
      
      _face_normals[i+0]=v2.v[0];
      _face_normals[i+1]=v2.v[1];
      _face_normals[i+2]=v2.v[2];
      
      //printf("------------------------------------\n");
      //print_GLfloat3(&_vertices[i]);
      //print_GLfloat3(&_vertices[i+3]);
      //print_GLfloat3(&_vertices[i+6]);
      //print_GLfloat3(&_face_normals[i]);
   }
}

unsigned int sphere::eliminate_duplicate_lines(GLfloat *vertices, unsigned int count)
{
   //printf("GL_sphere - eliminating duplicate lines\n"); fflush(stdout);
   unsigned int tested_end=2;

   for(unsigned int untested=2;untested<(count);untested+=2) //first line we keep
   {
      int duplicate_found=0;
      GLfloat *untested_1=&vertices[untested*3];
      GLfloat *untested_2=&vertices[(untested+1)*3];

      for(unsigned int e=0;e<tested_end;e+=2)
      {
         GLfloat *tested_1=&vertices[e*3];
         GLfloat *tested_2=&vertices[(e+1)*3];

         if( (is_GLfloat3_equal(untested_1,tested_1) &&
              is_GLfloat3_equal(untested_2,tested_2))  ||
             (is_GLfloat3_equal(untested_1,tested_2) &&
              is_GLfloat3_equal(untested_2,tested_1)) )
         {
            duplicate_found=1;
            break;
         }
      }
      if(duplicate_found==0)
      {
         //copy to next unused spot
         copy_GLfloat3(&vertices[(tested_end+0)*3],&vertices[(untested+0)*3]); 
         copy_GLfloat3(&vertices[(tested_end+1)*3],&vertices[(untested+1)*3]); 
         
         tested_end+=2;
      }
   }


   return tested_end;
}

/****************************************************************************/
/****************************************************************************/
/* USER CALLABLE                                                            */
/****************************************************************************/
/****************************************************************************/
sphere::sphere(int subdivision_factor)
{
   _factor=subdivision_factor;
   alloc_memory_for_vertices();

   printf("GL_sphere - initializing sphere - faces: %d  vertices: %d\n",
      _face_count,_vert_count);

   for(int i=0;i<20;i++)
      add_original_triangle(i, _factor);

   if(_current_pos!=_vert_count) //did we create the proper amount?
   {
      printf("GL_sphere - error! created: %d want: %d\n",_current_pos,_vert_count);
      exit(1);
   }
   
   printf("GL_sphere - calcing face normals\n");
   calc_face_normals();

   printf("GL_sphere - initializing sphere outline - vertices: %d.\n",_line_vert_count);
   calc_sphere_lines();

   unsigned int unique=eliminate_duplicate_lines(_line_vertices,_line_vert_count);
   if(unique != _vert_count) // 50%
   {
      printf("GL_sphere - error: didn't find proper number of duplicates!\n");
      exit(1);
   }
}

sphere::~sphere()
{
   free(_vertices);
   free(_line_vertices);
}

void sphere::draw()
{
   unsigned int max=_vert_count*3; //pull out of loop

   glBegin(GL_TRIANGLES);
      for (unsigned int i = 0; i < max; i+=3)
      { 
         glNormal3fv(&_vertices[i]);
         glVertex3fv(&_vertices[i]);
      }
   glEnd();
}

void sphere::draw_expanded(float amount)
{
   unsigned int max=_vert_count*3; //pull out of loop

   //printf("in draw_expanded: %f\n",expansion);
   glBegin(GL_TRIANGLES);
      for (unsigned int i = 0; i < max; i+=9)
      { 
         /*glNormal3fv(&_vertices[i]);
         glVertex3fv(&_vertices[i]);
         
         glNormal3fv(&_vertices[i+3]);
         glVertex3fv(&_vertices[i+3]);
         
         glNormal3fv(&_vertices[i+6]);
         glVertex3fv(&_vertices[i]);*/
         
         float x_expand=_face_normals[i+0]*amount;
         float y_expand=_face_normals[i+1]*amount;
         float z_expand=_face_normals[i+2]*amount;
         
         glNormal3fv(&_vertices[i]);
         glVertex3f(_vertices[i+0] + x_expand,_vertices[i+1] + y_expand,_vertices[i+2] + z_expand);
         
         glNormal3fv(&_vertices[i+3]);
         glVertex3f(_vertices[i+3] + x_expand,_vertices[i+4] + y_expand,_vertices[i+5] + z_expand);

         glNormal3fv(&_vertices[i+6]);
         glVertex3f(_vertices[i+6] + x_expand,_vertices[i+7] + y_expand,_vertices[i+8] + z_expand);


      }
   glEnd();
}

void sphere::draw_outline()
{
   unsigned int max=_vert_count*3; //pull out of loop

   glBegin(GL_LINES);
      for (unsigned int i = 0; i < max; i+=3)
      {
         glNormal3fv(&_line_vertices[i]);
         glVertex3fv(&_line_vertices[i]);
      }
   glEnd();
}

void generate_sphere_display_list(GLuint &whandle,GLuint &shandle, int division)
{
   sphere *s=new sphere(division);

   whandle =glGenLists(1);
   printf("GL_sphere - wire sphere has display list handle: %d\n",whandle);

   glNewList(whandle,GL_COMPILE);
      s->draw_outline();
   glEndList();

   shandle =glGenLists(1);
   printf("GL_sphere - sphere has display list handle: %d\n",shandle);

   glNewList(shandle,GL_COMPILE);
      s->draw();
   glEndList();
   
   delete s;
}

GLuint sdl_w_handle;
GLuint sdl_handle;

void init_display_list_sphere(int division)
{
   generate_sphere_display_list(sdl_w_handle,sdl_handle, division);

}

void draw_display_list_sphere()
{
   glCallList(sdl_handle);
}

void draw_display_list_wireframe_sphere()
{
   glCallList(sdl_w_handle);
}


/////////////////////////////////////////
// float 3 util
/////////////////////////////////////////

void copy_GLfloat3(GLfloat *dest,const GLfloat *src)
{
   dest[0]=src[0]; dest[1]=src[1]; dest[2]=src[2];
} 

void copy_GLfloat3_double(GLfloat *dest,const double *src)
{
   dest[0]=(GLfloat)src[0]; 
   dest[1]=(GLfloat)src[1]; 
   dest[2]=(GLfloat)src[2];
} 

void store_GLfloat3(GLfloat a, GLfloat b, GLfloat c, GLfloat *dest)
{
   dest[0]=a; dest[1]=b; dest[2]=c;
} 

void normalize_GLfloat3(GLfloat *v)
{
   GLfloat dist=sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);

   v[0]/=dist;
   v[1]/=dist;
   v[2]/=dist;
}

int is_GLfloat3_equal(const GLfloat *a, const GLfloat *b)
{
   if(a[0]!=b[0]) return 0;
   if(a[1]!=b[1]) return 0;
   if(a[2]!=b[2]) return 0;
 
   return 1;
}

void print_GLfloat3(const GLfloat *v)
{
   printf("float 3: %f %f %f\n",v[0],v[1],v[2]);
}

void calc_GLfloat3_average(const GLfloat *a, const GLfloat *b, GLfloat *ret)
{
   ret[0]=0.5*a[0]+0.5*b[0];
   ret[1]=0.5*a[1]+0.5*b[1];
   ret[2]=0.5*a[2]+0.5*b[2];
}

void calc_GLfloat3_average3(const GLfloat *a, const GLfloat *b, const GLfloat *c, GLfloat *ret)
{
   ret[0]=(a[0]+b[0]+c[0])/3.0f;
   ret[1]=(a[1]+b[1]+c[1])/3.0f;
   ret[2]=(a[2]+b[2]+c[2])/3.0f;
}
