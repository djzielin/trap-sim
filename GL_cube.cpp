/*****************************************************************/
/* cube.cpp - originally from William R. Sherman's shapes.c file */
/*                                                               */
/* djzielin 3-9-2007                                             */
/*****************************************************************/
#include "arPrecompiled.h"
#include "GL_cube.h"

/*****************************************************************/
/*
   Cube (and pyramid) vertices.  Labeled 0-7 (cube) & 8 (top of pyramid).


             7_______ 4
             /|     /|        (+y)
            / | .8 / |         |
          0/__|___/3 |         |
           | 6|___|__|5        o---- (+x)
           | /    | /         /
           |/     |/         /
          1|______|2       (+z)

*/


float cube_vertices[9][3] = {			/* specifies the (x,y,z)   */
   { -1.0,  1.0,  1.0 },	/* 0 */	/* location of each vertex */
   { -1.0, -1.0,  1.0 },	/* 1 */
   {  1.0, -1.0,  1.0 },	/* 2 */
   {  1.0,  1.0,  1.0 },	/* 3 */
   {  1.0,  1.0, -1.0 },	/* 4 */
   {  1.0, -1.0, -1.0 },	/* 5 */
   { -1.0, -1.0, -1.0 },	/* 6 */
   { -1.0,  1.0, -1.0 },	/* 7 */
   {  0.0,  1.0,  0.0 },	/* 8 */
};

int cube_polygons[6][4] = {			/* specifices the vertices */
   { 0, 1, 2, 3 },	/* front  */	/* of each polygon         */
   { 3, 2, 5, 4 },	/* right  */
   { 4, 5, 6, 7 },	/* back   */  //{ 5, 6, 7, 4 },	wrong keep with other orientations
   { 7, 6, 1, 0 },	/* left   */  
   { 6, 5, 2, 1 },	/* bottom */
   { 0, 3, 4, 7 },	/* top    */
};

float cube_tex_coords[4][2] = {
   {1.0, 1.0},
   {1.0, 0.0},
   {0.0, 0.0},
   {0.0, 1.0}
};

int cube_line_index[16] = {0,1,2,3,0,7,6,1,2,5,6,7,4,3,4,5};

float cube_normals[6][3] = {			/* specifies the normal */
   {  0.0,  0.0,  1.0 }, /* front  ** of each polygon      */
   {  1.0,  0.0,  0.0 }, /* right  */
   {  0.0,  0.0, -1.0 }, /* back   */
   { -1.0,  0.0,  0.0 }, /* left   */
   {  0.0, -1.0,  0.0 }, /* bottom */
   {  0.0,  1.0,  0.0 }, /* top    */
};

void GL_draw_cube()
{
   for (int poly = 0; poly < 6; poly++)
   {
	  glBegin(GL_POLYGON);
         glNormal3fv(cube_normals[poly]);
         glTexCoord2f(0.0, 1.0);
         glVertex3fv(cube_vertices[cube_polygons[poly][0]]);
         glTexCoord2f(0.0, 0.0);
         glVertex3fv(cube_vertices[cube_polygons[poly][1]]);
         glTexCoord2f(1.0, 0.0);
         glVertex3fv(cube_vertices[cube_polygons[poly][2]]);
         glTexCoord2f(1.0, 1.0);
         glVertex3fv(cube_vertices[cube_polygons[poly][3]]);
      glEnd();
   }
}

void GL_draw_cube_wireframe()
{
   glNormal3f( 0.0, 1.0,  0.0); 
   glBegin(GL_LINE_STRIP);
      glVertex3fv(cube_vertices[0]);
      glVertex3fv(cube_vertices[1]);
      glVertex3fv(cube_vertices[2]);   
      glVertex3fv(cube_vertices[3]);
      glVertex3fv(cube_vertices[0]);
      glVertex3fv(cube_vertices[7]);
      glVertex3fv(cube_vertices[6]);
      glVertex3fv(cube_vertices[1]);
      glVertex3fv(cube_vertices[2]);
      glVertex3fv(cube_vertices[5]);
      glVertex3fv(cube_vertices[6]);
      glVertex3fv(cube_vertices[7]);
      glVertex3fv(cube_vertices[4]);
      glVertex3fv(cube_vertices[3]);
      glVertex3fv(cube_vertices[4]);
      glVertex3fv(cube_vertices[5]);       
   glEnd();
}

